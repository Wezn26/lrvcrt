# STEP 1 ENTER IN DOCKER CONTAINER WITH NAME 'php-fpm-lrvcrt'
```docker exec -it php-fpm-lrvcrt bash```
# STEP 2 IF YOU NEED GENERATE KEY
```php artisan key:generate```
# STEP 3 IF YOU CLONE REPOSITORY EXECUTE THIS COMMAND
```
composer install
npm install
npm run dev
```
# STEP 4 CREATE MODEL Post WITH MIGRATION
```php artisan make:model Post -m```

```
<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    use HasFactory;
    protected $table = 'posts';
}
```

# STEP 5 ADD CODE IN database/migration/2022_02_22_091303_create_posts_table.php
```
public function up()
    {
        Schema::create('posts', function (Blueprint $table) {
            $table->id();
            $table->string('title');
            $table->text('content');
            $table->string('image')->nullable();
            $table->unsignedBigInteger('likes')->nullable();
            $table->boolean('is_published')->default(1);
            $table->timestamps();
        });
    }
```
# STEP 6 CREATE PostController.php AND ADD public function index()
```php artisan make:controller PostController```
```
public function index()
    {
        dd('Hello From PostController!!!');
    }
```
# STEP 7 ADD ROUTE 'posts' IN web.php
```Route::get('posts', 'PostController@index');``` 

# STEP 8 CHANGE public function index() IN PostController.php
```
public function index()
    {
        $post = Post::find(1);
        $posts = Post::all();
        $isPublished = Post::where('is_published', 0)->first();
        dump($post->title);
        dump($post);
        dump($isPublished->title);
        foreach ($posts as $post) {
            dump($post->title);
        }
        dd($posts)
    }
```
# STEP 9 ADD ROUTE 'posts/create'
```Route::get('posts/create', 'PostController@create');```
# STEP 10 ADD public function create() IN PostController.php
```
public function create()
    {
        $postsArr  = [
            [
                'title' => 'first record',
                'content' => 'first record conten',
                'image' => 'first_image.jpg',
                'likes' => 10,
                'is_published' => 1
            ],
            [
                'title' => 'second record',
                'content' => 'second record conten',
                'image' => 'second_image.jpg',
                'likes' => 20,
                'is_published' => 1
            ],
            [
                'title' => 'third record',
                'content' => 'third record conten',
                'image' => 'third_image.jpg',
                'likes' => 30,
                'is_published' => 1
            ],
        ];

        foreach ($postsArr as $item) {
            //dd($item);
            Post::create($item);            
        }
        dd('CREATED!!!');
    }
}
```
# STEP 11 ADD ARRAY protected $guarded = [];
```
<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    use HasFactory;
    protected $table = 'posts';
    protected $guarded = false;
    //protected $guarded = [];
}
```
# STEP 12 ADD ROUTE 'post/update'
```Route::get('posts/update', 'PostController@update');```
# STEP 13 ADD public function update() IN PostController.php
```
public function update()
    {
        $post = Post::find(6);
        //dd($post);
        $post->update([
            'title' => 'updated',
            'content' => 'updated',
            'image' => 'updated.jpg',
            'likes' => 100,
            'is_published' => 0
        ]);
        dd('UPDATED!!!');
    }
```
# STEP 14 ADD ROUTE 'posts/delete'
```Route::get('posts/delete', 'PostController@delete');```
# STEP 15 ADD public function delete() IN PostController.php
```
public function delete()
    {
        $post = Post::find(6);
        $post->delete();
        dd('DELETED!!!');
    }
``` 
# STEP 16 ADD $table->softDeletes(); in create_posts_table.php
```$table->softDeletes();```

```
<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('posts', function (Blueprint $table) {
            $table->id();
            $table->string('title');
            $table->text('content');
            $table->string('image')->nullable();
            $table->unsignedBigInteger('likes')->nullable();
            $table->boolean('is_published')->default(1);
            $table->timestamps();

            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('posts');
    }
};
```
# STEP 17 ADD TRAIT SoftDeletes IN MODEL Post.php
```
<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Post extends Model
{
    use HasFactory;
    use SoftDeletes;
    protected $table = 'posts';
    protected $guarded = false;
    //protected $guarded = [];
}
```
# STEP 18 REFRESH ALL MIGRATIONS
```php artisan migrate:fresh```
# STEP 19 ADD ROUTE 'posts/restore'
```Route::get('posts/restore', 'PostController@restore');```
# STEP 20 ADD public function restore() IN PostController.php
```
public function restore()
    {
        $post = Post::withTrashed()->find(6);
        $post->restore();
        dd('RESTORED!!!');
    }
```
# STEP 21 CREATE ROUTE 'posts/first_or_create'
```Route::get('posts/first_or_create', 'PostController@firstOrCreate');```
# STEP 22 ADD public function firstOrCreate() IN PostController.php
```
public function firstOrCreate()
    {
        $anotherPost = [
            'title' => 'another record',
            'content' => 'another record content',
            'image' => 'another_image.jpg',
            'likes' => 50,
            'is_published' => 1
        ];

        $post = Post::firstOrCreate(
            [
                'title' => 'some title'
            ],
            $anotherPost
        );
        dump($post->content);
        dd('FINISHED!!!');
    }
```
# STEP 23 CREATE ROUTE 'posts/update_or_create'
```Route::get('posts/update_or_create', 'PostController@updateOrCreate');```
# STEP 24 ADD public function updateOrCreate() IN PostController.php
```
public function updateOrCreate()
    {
        $anotherPost = [
            'title' => 'some title',
            'content' => 'new record content',
            'image' => 'new_image.jpg',
            'likes' => 150,
            'is_published' => 0
        ];

        $post = Post::updateOrCreate(
            [
                'title' => 'some title'
            ],
            $anotherPost
        );

        dump($post->content);
        dd('FINISHED!!!');

    }
```
# STEP 25 CREATE MIGRATION ADD COLUMN WITH NAME 'add_column_description_to_posts_table'
```php artisan make:migration add_column_description_to_posts_table```

```
<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('posts', function (Blueprint $table) {
            $table->text('description')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('posts', function (Blueprint $table) {
            $table->dropColumn('description');
        });
    }
};
```
# STEP 26 EXECUTE ALL MIGRATIONS
```php artisan migrate```

# STEP 27 ROLLBACK LAST MIGRATION
```php artisan migrate:rollback```

# STEP 28 CHANGE public function up() IN add_column_description_to_posts_table
```
public function up()
    {
        Schema::table('posts', function (Blueprint $table) {
            $table->text('description')->nullable()->after('content');
        });
    }
``` 
# STEP 29  EXECUTE ALL MIGRATIONS
```php artisan migrate```  
# STEP 30 CREATE MIGRATION delete_column_description_to_posts_table
```php artisan make:migration delete_column_description_to_posts_table```
```
<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('posts', function (Blueprint $table) {
            $table->dropColumn('description');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('posts', function (Blueprint $table) {
            $table->text('description')->nullable()->after('content');
        });
    }
};
```
# STEP 31 EXECUTE ALL MIGRATIONS DELETE COLUMN description
```php artisan migrate```
# STEP 32 CREATE MIGRATION edit_column_content_to_posts_table
```php artisan make:migration edit_column_content_to_posts_table```
```
<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('posts', function (Blueprint $table) {
            $table->renameColumn('content', 'post_content');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('posts', function (Blueprint $table) {
            $table->renameColumn('post_content', 'content');
        });
    }
};
```
# STEP 33 EXECUTE ALL MIGRATIONS RENAME COLUMN content ON post_content
```php artisan migrate```
# STEP 34 CHANGE DATA TYPE IN COLUMN post_content CREATE MIGRATION change_column_post_content_string_to_posts_table
```php artisan make:migration change_column_post_content_string_to_posts_table```
```
<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('posts', function (Blueprint $table) {
            $table->string('post_content')->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('posts', function (Blueprint $table) {
            $table->string('post_content')->change();
        });
    }
};
```
# STEP 35 EXECUTE ALL MIGRATIONS CHANGE DATA TYPE COLUMN post_content
```php artisan migrate```
# STEP 36 DELETE DATABASE TABLE IF NEED IT CREATE MIGRATION delete_table_from_db
```php artisan make:migration delete_table_from_db```
# STEP 37 CREATE public function viewPosts() IN PostController.php
```
public function viewPosts()
    {
        $posts = Post::all();
        return view('posts', compact('posts'));
    }
```
# STEP 38 CREATE posts.blade.php IN resources/views  
```
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Posts</title>
</head>
<body>
    @foreach ($posts as $post)
        <div>{{ $post->title }}</div>
        <div>{{ $post->post_content }}</div>
    @endforeach
</body>
</html>
```
# STEP 39 CREATE VIEW 'posts/view' IN web.php
```Route::get('posts/view', 'PostController@viewPosts');```
# STEP 40 CREATE  contacts.blade.php about.blade.php main.blade.php
```
@extends('layouts.main')
@section('content')
        <h1 style="background: rgb(128, 51, 0)">
            This is main page
        </h1>
@endsection
```
# STEP 41 CHANGE posts.blade.php
```
@extends('layouts.main')
@section('content')
    <h1 style="background: purple">
        This is posts page
    </h1>
    @foreach ($posts as $post)
        <div style="background: red">{{ $post->title }}</div>
        <div style="background: yellow">{{ $post->post_content }}</div>
    @endforeach
@endsection
```
# STEP 42 CREATE FOLDER layouts IN resources/views AND CREATE INTO FILE main.blade.php
```
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Layout main page</title>
</head>
<body>
    <div>
        @yield('content')
    </div>
</body>
</html>
```
# STEP 43 LOOK AT 'posts/view'

# STEP 44 CREATE ROUTES 'main' 'contacts' 'about' AND ADD name('post.index') route 'posts/view'
```
Route::get('posts/view', 'PostController@viewPosts')->name('post.view');
```
```
//Lesson15
Route::get('main', 'MainController@index')->name('main.index');
Route::get('contacts', 'ContactController@index')
->name('contact.index');
Route::get('about', 'AboutController@index')->name('about.index');
```
# STEP 45 CREATE CONTROLLERS MainController.php ContactController.php AboutController.php
```php artisan make:controller MainController```
```php artisan make:controller ContactController```
```php artisan make:controller AboutController```
# STEP 46 ADD public function index() IN MainController.php
```
public function index()
    {
        return view('main');
    }
```
# STEP 47 ADD public function index() IN ContactController.php
```
public function index()
    {
        return view('contacts');
    }
```
# STEP 48 ADD public function index() IN AboutController.php
```
public function index()
    {
        return view('about');
    }
```
# STEP 49 ADD NAVIGATION IN layouts/main.blade.php 
```
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Layout main page</title>
</head>
<body>
    <nav>
        <ul>
            <li><a href="{{ route('main.index') }}">Main</a></li>
            <li><a href="{{ route('post.view') }}">Posts</a></li>
            <li><a href="{{ route('about.index') }}">About</a></li>
            <li><a href="{{ route('contact.index') }}">Contacts</a></li>            
        </ul>
    </nav>
    <div>
        @yield('content')
    </div>
</body>
</html>
```
# STEP 50 LOOK AT 'http://localhost:81/main'
# STEP 51 INSTALL laravel/ui
```composer require laravel/ui```
# STEP 52 CHECK INSTALLATION
```php artisan```
# STEP 53 INSTALL BOOTSTRAP
```php artisan ui bootstrap```
# STEP 54 RUN THESE COMMANDS
```npm install```
```npm run dev```
# STEP 55 ADD LINK IN lauouts/main.blade.php AND ADD nav class='container' ul class='row'
```<link rel="stylesheet" href="{{ asset('css/app.css') }}">```
```
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
    <title>Layout main page</title>
</head>
<body>
    <nav class="container">
        <ul class="row">
            <li><a href="{{ route('main.index') }}">Main</a></li>
            <li><a href="{{ route('post.view') }}">Posts</a></li>
            <li><a href="{{ route('about.index') }}">About</a></li>
            <li><a href="{{ route('contact.index') }}">Contacts</a></li>            
        </ul>
    </nav>
    <div>
        @yield('content')
    </div>
</body>
</html>
```
# STEP 56 ADD BOOTSTARP TABLE IN posts.blade.php
```
@extends('layouts.main')
@section('content')
    <h1 style="background: purple">
        This is posts page
    </h1>
    @foreach ($posts as $post)
        <div style="background: red">{{ $post->title }}</div>
        <div style="background: yellow">{{ $post->post_content }}</div>
    @endforeach
    <table class="table">
  <thead>
    <tr>
      <th scope="col">#</th>
      <th scope="col">First</th>
      <th scope="col">Last</th>
      <th scope="col">Handle</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th scope="row">1</th>
      <td>Mark</td>
      <td>Otto</td>
      <td>@mdo</td>
    </tr>
    <tr>
      <th scope="row">2</th>
      <td>Jacob</td>
      <td>Thornton</td>
      <td>@fat</td>
    </tr>
    <tr>
      <th scope="row">3</th>
      <td>Larry</td>
      <td>the Bird</td>
      <td>@twitter</td>
    </tr>
  </tbody>
</table>
@endsection
```
# STEP 57 ADD BOOTSTRAP NAVBAR IN layouts/main.blade.php
```
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
    <title>Layout main page</title>
</head>
<body>   

    <div class="container">
        <div class="row">
            <nav class="navbar navbar-expand-lg navbar-light bg-light">
  
  <div class="collapse navbar-collapse" id="navbarNav">
    <ul class="navbar-nav">
      <li class="nav-item active">
        <a class="nav-link" href="{{ route('main.index') }}">Main <span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="{{ route('post.view') }}">Posts</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="{{ route('about.index') }}">About</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="{{ route('contact.index') }}">Contacts</a>
      </li>
      
    </ul>
  </div>
</nav>
        </div>
    </div>

    <div>
        @yield('content')
    </div>
</body>
</html>
```
# STEP 58 CREATE FOLDER post AND ADD INTO index.blade.php

# STEP 59 CREATE MyPostController.php
```php artisan make:controller MyPostController```
```
<?php

namespace App\Http\Controllers;

use App\Models\Post;
use Illuminate\Http\Request;

class MyPostController extends Controller
{
    public function index()
    {
        $posts = Post::all();
        return view('post.index', compact('posts'));
    }
}
```
# STEP 60 ADD ROUTE 'myposts'
```
// Lesson 17
Route::get('myposts', 'MyPostController@index')->name('mypost.index');
```
# STEP 61 ADD CODE IN post/index.blade.php
```
@extends('layouts.myposts')
@section('myposts')
    <div>
        <div>           
                <a href="{{ route('mypost.create') }}" 
                   class="btn btn-primary mb-3">
                    Create Post
                </a>                       
        </div>
        @foreach ($posts as $post)
            <div style="background: rgb(63, 78, 31)">
            <a href="{{ route('mypost.show', $post->id) }}">
              <b>{{ $post->id }}</b> . {{ $post->title }}
            </a> 
            </div>
        @endforeach
    </div>
@endsection

```
# STEP 62 CREATE layouts/myposts.blade.php
```
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
    <title>Test</title>
</head>
<body>
    <div>
        @yield('myposts')
    </div>
</body>
</html>
```
# STEP 63 ADD post/create.blade.php
```
@extends('layouts.myposts')
@section('myposts')

<div class="container">
    <div class="row">
        <h1 class="alert alert-warning">Create Posts</h1>
<form action="{{ route('mypost.store') }}" method="POST">
    @csrf
  <div class="form-group">
      <label for="title">Title</label>
      <input type="text" name="title" class="form-control" id="title" placeholder="Title">
  </div>

  <div class="form-group">
      <label for="content">Content</label>
      <textarea name="content" class="form-control" id="content" placeholder="Content"></textarea>
  </div>

  <div class="form-group">
      <label for="image">Image</label>
      <input type="text" name="image" class="form-control" id="image" placeholder="Image">
  </div>  

  <button type="submit" class="btn btn-primary">Create</button>
</form>
    </div>
</div>

@endsection

```
# STEP 64 ADD public function create() IN MyPostController.php
```
public function create()
    {
        return view('post.create');
    }
```
# STEP 65 ADD ROUTE 'myposts/create'
```Route::get('myposts/create', 'MyPostController@create')->name('mypost.create');```
# STEP 66 ADD ROUTE POST 'mypost'    
```Route::post('myposts', 'MyPostController@store')->name('mypost.store');```
# STEP 67 CREATE public function store() IN MyPostController.php
```
public function store()
    {
        $data = request()->validate([
            'title' => 'string',
            'content' => 'string',
            'image' => 'string'            
        ]);
        Post::create($data);
        return redirect()->route('mypost.index');
    }
```
# STEP 68 ADD ROUTE 'myposts/{id}'
```
Route::get('myposts/{post}', 'MyPostController@show')->name('mypost.show');
```
# STEP 69 CREATE public function show($id) IN MyPostController.php SHOW THIS http://localhost:81/myposts/2
```
    public function show($id)
    {
        $post = Post::findOrFail($id);
        dd($post->title);
    }
```
```
    public function show(Post $post)
    {  
        return view('post.show', compact('post'));      
        //dd($post->title);
    }
```    
# STEP 70 CREATE post/show.blade.php
```
@extends('layouts.myposts')
@section('myposts')
    <div>        
            <div style="background: yellowgreen">
              <b>{{ $post->id }}</b> . {{ $post->title }}
            </div>
            <div style="background: olive">{{ $post->content }}</div>
        
    </div>
    <div>
      <h3>
        <a href="{{ route('mypost.index') }}">Back</a>
      </h3>      
    </div>
@endsection
```
# STEP 71 ADD ROUTE 'myposts/{post}/edit'
```
Route::get('myposts/{post}/edit', 'MyPostController@edit')
->name('mypost.edit');
```
# STEP 72 ADD public function edit(Post $edit) IN MyPostController.php
```
    public function edit(Post $post)
    {
        return view('post.edit', compact('post'));
    }
```
# STEP 73 CREATE post/edit.blade.php    
```
@extends('layouts.myposts')
@section('myposts')

<div class="container">
    <div class="row">
        <h1 class="alert alert-warning">Create Posts</h1>
<form action="{{ route('mypost.update', $post->id) }}" method="POST">
    @csrf
    @method('patch')
  <div class="form-group">
      <label for="title">Title</label>
      <input type="text" name="title" class="form-control" id="title" placeholder="Title" value="{{ $post->title }}">
  </div>

  <div class="form-group">
      <label for="content">Content</label>
      <textarea name="content" class="form-control" id="content" placeholder="Content">{{ $post->content }}</textarea>
  </div>

  <div class="form-group">
      <label for="image">Image</label>
      <input type="text" name="image" class="form-control" id="image" placeholder="Image" value="{{ $post->image }}">
  </div>  

  <button type="submit" class="btn btn-primary">Update</button>
</form>
    </div>
</div>

@endsection

```
# STEP 74 ADD ROUTE PATCH 'mypost/{post}'
```
Route::patch('myposts/{post}', 'MyPostController@update')
->name('mypost.update');
```
# STEP 75 ADD public function update(Post $post) IN MyPostController.php
```
public function update(Post $post)
    {
        $data = request()->validate([
            'title' => 'string',
            'content' => 'string',
            'image' => 'string'
        ]);
        $post->update($data);
        return redirect()->route('mypost.show', $post->id);
    }
```
# STEP 76 ADD LINK IN post/show.blade.php
```
<div>
      <h3>
        <a href="{{ route('mypost.edit', $post->id) }}">Edit</a>
      </h3>      
</div>
```
```
@extends('layouts.myposts')
@section('myposts')
    <div>      
            <div style="background: yellowgreen">
              <b>{{ $post->id }}</b> . {{ $post->title }}
            </div>
            <div style="background: olive">
              {{ $post->content }}
            </div>    
    </div>

    <div>
      <h3>
        <a href="{{ route('mypost.edit', $post->id) }}">Edit</a>
      </h3>      
    </div>

    <div>
      <h3>
        <a href="{{ route('mypost.index') }}">Back</a>
      </h3>      
    </div>
@endsection
```
# STEP 77 ADD ROUTE DELETE 'myposts/{post}'
```
Route::delete('myposts/{post}', 'MyPostController@destroy')
->name('mypost.delete');
```
# STEP 78 ADD public function destroy(Post $post)
```
    public function destroy(Post $post)
    {
        $post->delete();
        return redirect()->route('mypost.index');
    }
```
# STEP 79  ADD LINK Delete IN post/show.blade.php  
```
@extends('layouts.myposts')
@section('myposts')
    <div>      
            <div style="background: yellowgreen">
              <b>{{ $post->id }}</b> . {{ $post->title }}
            </div>
            <div style="background: olive">
              {{ $post->content }}
            </div>    
    </div>

    <div>
      <h3>
        <a href="{{ route('mypost.edit', $post->id) }}">Edit</a>
      </h3>      
    </div>

    <div>
      <form action="{{ route('mypost.delete', $post->id) }}"        method="post">
        @csrf
        @method('delete')
        <input type="submit" value="Delete" class="btn btn-danger">
      </form>
      <h3>
        <a href="">Delete</a>
      </h3>      
    </div>

    <div>
      <h3>
        <a href="{{ route('mypost.index') }}">Back</a>
      </h3>      
    </div>
@endsection
```
# STEP 80 START LESSON 18 CREATE MODEL Category WITH MIGRATION
```php artisan make:model Category -m```
# STEP 81 ADD COLUMN $table->string('title') IN database/migrations/ create_categories_table.php
```
<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('categories', function (Blueprint $table) {
            $table->id();
            $table->string('title');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('categories');
    }
};
```
# STEP 82 CREATE MIGRATION 
```php artisan make:migration add_foreign_key_to_posts_table```
# STEP 83 ADD CODE IN database/migrations/add_foregn_key_to_posts_table.php
```
<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('posts', function (Blueprint $table) {
            $table->unsignedBigInteger('category_id')->nullable();
            $table->index('category_id', 'post_category_idx');
            $table->foreign('category_id', 'post_category_fk')->on('categories')->references('id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('posts', function (Blueprint $table) {
            $table->dropColumn('category_id');
        });
    }
};
```
# STEP 84 FRESH ALL MIGRATIONS
```php artisan migrate:fresh```
# STEP 85 ADD MANUALLY DATA IN TABLE 'categories' IN FIELD 'title'
```
id  title
1   cats
2   dogs
```
# STEP 86 ADD MANUALLY DATA IN TABLE 'posts'
```
id  title  content       image      likes is_published  category_id
1   cats   cats content  cats.jpg     10      1               1
2   dogs   dogs content  dogs.jpg     20      1               2
3   puma   puma content  puma.jpg     30      1               1
```
# STEP 87 CREATE AnimalController.php
```php artisan make:controller AnimalController```
# STEP 88 ADD ROUTE 'animals' IN web.php
```
Route::get('animals', 'AnimalController@index')->name('animal.index');
```
# STEP 89 ADD public function index IN AnimalController.php
```
<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Post;
use Illuminate\Http\Request;

class AnimalController extends Controller
{
    public function index()
    {        
        $categories = Category::all();
        $category = Category::find(1);
        $posts = Post::where('category_id', $category->id)->get();
        $post = Post::find(1);
        echo 'All categories!!!';
        dump($categories); 
        echo 'Category Title!!!';
        dump($category->title);
        echo 'Posts Category!!!';
        dump($posts);

        //Main Realisation
        echo 'All The Same!!!';
        dump($category->posts);
        echo 'Post Category!!!';
        dump($post->category);
    }
}

```
# STEP 90 FIXES ERROR 'Does not exists AnimalController::index'
```
 1. composer update 
 2. composer dumpautoload
 3. php artisan config:cache
 4. php artisan view:clear
```
# STEP 91 ADD public function posts() IN Model/Category.php
```
<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    use HasFactory;

    public function posts()
    {
        return $this->hasMany(Post::class, 'category_id', 'id');
    }
}
```
# STEP 92 ADD public function category() IN Models/Post.php
```
<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Post extends Model
{
    use HasFactory;
    use SoftDeletes;
    protected $table = 'posts';
    protected $guarded = false;
    //protected $guarded = [];

    public function category()
    {
        return $this->belongsTo(Category::class, 'category_id', 'id');
    }
}
```
# STEP 94 START LESSON 19 DELETE THESE MIGRATIONS
### 1
```2022_02_22_091303_create_posts_table.php```

```
<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('posts', function (Blueprint $table) {
            $table->id();
            $table->string('title');
            $table->text('content');
            $table->string('image')->nullable();
            $table->unsignedBigInteger('likes')->nullable();
            $table->boolean('is_published')->default(1);
            $table->timestamps();

            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('posts');
    }
};
```
### 2
```2022_03_05_163123_add_column_description_to_posts_table.php```

```
<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('posts', function (Blueprint $table) {
            $table->text('description')->nullable()->after('content');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('posts', function (Blueprint $table) {
            $table->dropColumn('description');
        });
    }
};
```
### 3
```2022_03_12_101813_delete_column_description_to_posts_table.php```

```
<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('posts', function (Blueprint $table) {
            $table->dropColumn('description');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('posts', function (Blueprint $table) {
            $table->text('description')->nullable()->after('content');
        });
    }
};
```
### 4
```2022_03_12_102845_edit_column_content_to_posts_table.php```

```
<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('posts', function (Blueprint $table) {
            $table->renameColumn('content', 'post_content');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('posts', function (Blueprint $table) {
            $table->renameColumn('post_content', 'content');
        });
    }
};
```
### 5
```2022_03_12_105659_change_column_post_content_string_to_posts_table.php```

```
<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('posts', function (Blueprint $table) {
            $table->string('post_content')->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('posts', function (Blueprint $table) {
            $table->string('post_content')->change();
        });
    }
};
```
### 6
```2022_03_12_164812_delete_table_from_db.php```

```
<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('test');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::create('test', function (Blueprint $table) {
            $table->id();
            $table->text('content');
        });
    }
};
```
### 7
```2022_04_12_102007_create_categories_table.php```

```
<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('categories', function (Blueprint $table) {
            $table->id();
            $table->string('title');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('categories');
    }
};
```
### 8
```2022_04_12_104021_add_foreign_key_to_posts_table.php```

```
<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('posts', function (Blueprint $table) {
            $table->unsignedBigInteger('category_id')->nullable();
            $table->index('category_id', 'post_category_idx');
            $table->foreign('category_id', 'post_category_fk')->on('categories')->references('id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('posts', function (Blueprint $table) {
            $table->dropColumn('category_id');
        });
    }
};
```
# STEP 95 CREATE MIGRATION create_posts_table AND ADD CONTENT
```php artisan make:migration create_posts_table```

```
<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('posts', function (Blueprint $table) {
            $table->id();
            $table->string('title');
            $table->text('content');
            $table->string('image')->nullable();
            $table->unsignedBigInteger('likes')->nullable();
            $table->boolean('is_published')->default(1);
            $table->timestamps();

            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('posts');
    }
};
```
# STEP 96 CREATE MIGRATION create_categories_table AND ADD $table->string('title')
```php artisan make:migration create_categories_table```

```
<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('categories', function (Blueprint $table) {
            $table->id();
            $table->string('title');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('categories');
    }
};
```
# STEP 97 EXECUTE ALL MIGRATIONS
```php artisan migrate```
# STEP 98 ADD MANUALLY DATA IN TABLE 'posts'
```
id  title  content       image      likes is_published  category_id
1   cats   cats content  cats.jpg     10      1               1
2   dogs   dogs content  dogs.jpg     20      1               2
3   puma   puma content  puma.jpg     30      1               1
```
# STEP 99 ADD MANUALLY DATA IN TABLE 'categories' IN FIELD 'title'
```
id  title
1   cats
2   dogs
```
# STEP 100 CREATE MODEL Tag WITH MIGRATION AND ADD $table->string('title')
```php artisan make:model Tag -m```

```
<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tags', function (Blueprint $table) {
            $table->id();
            $table->string('title');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tags');
    }
};
```
# STEP 101 CREATE MODEL PostTag WITH MIGRATION
```php artisan make:model PostTag -m```
# STEP 102 ADD CODE IN MIGRATION create_posts_tags_table.php
```
<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('post_tags', function (Blueprint $table) {
            $table->id();

            $table->unsignedBigInteger('post_id');
            $table->unsignedBigInteger('tag_id');

            $table->index('post_id', 'post_tag_post_idx');
            $table->index('tag_id', 'post_tag_tag_idx');

            $table->foreign('post_id', 'post_tag_post_fk')
            ->on('posts')->references('id');
            $table->foreign('tag_id', 'post_tag_tag_fk')
            ->on('tags')->references('id');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('post_tags');
    }
};
```
# STEP 103 EXECUTE ALL MIGRATIONS
```php artisan migrate```
# STEP 104 ADD CONTENT MANUALLY IN TABLE tags
```
id   title
1    holidays
2    travel
3    pizza
```
# STEP 105 CREATE MIGRATION add_foreign_key_to_posts_table AND ADD CONTENT
```php artisan make:migration add_foreign_key_to_posts_table```

```
<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('posts', function (Blueprint $table) {
            $table->unsignedBigInteger('category_id')->nullable();
            $table->index('category_id', 'post_category_idx');
            $table->foreign('category_id', 'post_category_fk')
            ->on('categories')->references('id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('posts', function (Blueprint $table) {
            $table->dropColumn('category_id');
        });
    }
};
```
# STEP 106 EXECUTE ALL MIGRATIONS
```php artisan migrate```
# STEP 107 ADD MANUALLY DATA IN TABLE 'posts' id=4
```
id  title    content         image      likes is_published  category_id
1   cats     cats content    cats.jpg     10      1               1
2   dogs     dogs content    dogs.jpg     20      1               2
3   puma     puma content    puma.jpg     30      1               1
4   holiday  holiday content holiday.jpg  40      1               2
```
# STEP 108 ADD MANUALLY TAGS IN TABLE post_tags
```
id  post_id   tag_id    created_at    updated_at
1      1         1
2      1         3
3      2         2
4      2         1
5      3         1
6      3         1
7      3         1
``` 
# STEP 109 ADD public function tags() IN Models/Post.php
```
    public function tags()
    {
        return $this->belongsToMany(Tag::class, 'post_tags', 'post_id', 'tag_id');
    }
```

```
<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Post extends Model
{
    use HasFactory;
    use SoftDeletes;
    protected $table = 'posts';
    protected $guarded = false;
    //protected $guarded = [];

    public function category()
    {
        return $this->belongsTo(Category::class, 'category_id', 'id');
    }

    public function tags()
    {
        return $this->belongsToMany(Tag::class, 'post_tags', 'post_id', 'tag_id');
    }
}
```
# STEP 110 ADD echo 'Tags' AND 'Post_Tags' IN AnimalController.php LOOK AT http://localhost:81/animals
```
$tag = Tag::find(1);

echo 'Post_Tags';
dump($tag->posts);

echo 'Tags';
dump($post->tags);
```
```
<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Post;
use App\Models\Tag;
use Illuminate\Http\Request;

class AnimalController extends Controller
{
    public function index()
    {        
        $categories = Category::all();
        $category = Category::find(1);
        $posts = Post::where('category_id', $category->id)->get();
        $post = Post::find(1);
        $tag = Tag::find(1);
        echo 'All categories!!!';
        dump($categories); 
        echo 'Category Title!!!';
        dump($category->title);
        echo 'Posts Category!!!';
        dump($posts);
        echo 'Tags';
        dump($post->tags);
        echo 'Post_Tags';
        dump($tag->posts);

        //Main Realisation
        echo 'All The Same!!!';
        dump($category->posts);
        echo 'Post Category!!!';
        dump($post->category);
    }
}
```
# STEP 111 FINISHED LESSON 19 ADD public function posts() IN Models/Tag.php LOOK AT http://localhost:81/animals 

```
<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
    use HasFactory;

    public function posts()
    {
        return $this->belongsToMany(Post::class, 'post_tags', 'tag_id', 'post_id');
    }
}
```
# STEP 112 START LESSON 20 ADD BOOTSRAP FORM IN /resources/views/post/create.blade.php
```
<div class="form-group">
    <label for="category">Category</label>
    <select class="form-control" id="category" name="category_id">
        @foreach ($categories as $category)
            <option value="{{ $category->id }}">{{ $category->title }}</option>
        @endforeach           
    </select>
</div>
```

```
@extends('layouts.myposts')
@section('myposts')

<div class="container">
    <div class="row">
        <h1 class="alert alert-warning">Create Posts</h1>
<form action="{{ route('mypost.store') }}" method="POST">
    @csrf
  <div class="form-group">
      <label for="title">Title</label>
      <input type="text" name="title" class="form-control" id="title" placeholder="Title">
  </div>

  <div class="form-group">
      <label for="content">Content</label>
      <textarea name="content" class="form-control" id="content" placeholder="Content"></textarea>
  </div>

  <div class="form-group">
      <label for="image">Image</label>
      <input type="text" name="image" class="form-control" id="image" placeholder="Image">
  </div>
  
  <div class="form-group">
    <label for="category">Category</label>
    <select class="form-control" id="category" name="category_id">
        @foreach ($categories as $category)
            <option value="{{ $category->id }}">{{ $category->title }}</option>
        @endforeach           
    </select>
  </div>

  <button type="submit" class="btn btn-primary">Create</button>
</form>
    </div>
</div>

@endsection
```
# STEP 113 CHANGE public function create() AND store() IN MyPostController.php LOOK AT: http://localhost:81/myposts
```
public function create()
    {
        $categories = Category::all();
        return view('post.create', compact('categories'));
    }

    public function store()
    {
        $data = request()->validate([
            'title'       => 'string',
            'content'     => 'string',
            'image'       => 'string',
            'category_id' => ''            
        ]);
        Post::create($data);
        return redirect()->route('mypost.index');
    }

```
# STEP 114 ADD BOOTSRAP FORM IN /resources/views/post/edit.blade.php
```
<div class="form-group">
    <label for="category">Category</label>
    <select class="form-control" id="category" name="category_id">
        @foreach ($categories as $category)
            <option
            {{ $category->id === $post->category->id ? ' selected' : ''}}
             value="{{ $category->id }}">{{ $category->title }}</option>
        @endforeach           
    </select>
  </div>
```

```
@extends('layouts.myposts')
@section('myposts')

<div class="container">
    <div class="row">
        <h1 class="alert alert-warning">Update Posts</h1>
<form action="{{ route('mypost.update', $post->id) }}" method="POST">
    @csrf
    @method('patch')
  <div class="form-group">
      <label for="title">Title</label>
      <input type="text" name="title" class="form-control" id="title" placeholder="Title" value="{{ $post->title }}">
  </div>

  <div class="form-group">
      <label for="content">Content</label>
      <textarea name="content" class="form-control" id="content" placeholder="Content">{{ $post->content }}</textarea>
  </div>

  <div class="form-group">
      <label for="image">Image</label>
      <input type="text" name="image" class="form-control" id="image" placeholder="Image" value="{{ $post->image }}">
  </div>
  
  <div class="form-group">
    <label for="category">Category</label>
    <select class="form-control" id="category" name="category_id">
        @foreach ($categories as $category)
            <option
            {{ $category->id === $post->category->id ? ' selected' : ''}}
             value="{{ $category->id }}">{{ $category->title }}</option>
        @endforeach           
    </select>
  </div>


  <button type="submit" class="btn btn-primary">Update</button>
</form>
    </div>
</div>

@endsection
```
# STEP 115 FINISHED LESSON 20 CHANGE public function edit() IN MyPostController.php
```
public function edit(Post $post)
{
    $categories = Category::all();
    return view('post.edit', compact('post', 'categories'));
}
```
# STEP 116 START LESSON 21 ADD BOOTSTRAP MULTIPLE SELECT IN /resources/views/post/create.blade.php
```
<div class="form-group">
    <label for="tags">Tags</label>
    <select multiple class="form-control" id="tags" name="tags[]">
        @foreach ($tags as $tag)
           <option value="{{ $tag->id }}">{{ $tag->title }}</option>  
        @endforeach           
    </select>
  </div>
```  
```
@extends('layouts.myposts')
@section('myposts')

<div class="container">
    <div class="row">
        <h1 class="alert alert-warning">Create Posts</h1>
<form action="{{ route('mypost.store') }}" method="POST">
    @csrf
  <div class="form-group">
      <label for="title">Title</label>
      <input type="text" name="title" class="form-control" id="title" placeholder="Title">
  </div>

  <div class="form-group">
      <label for="content">Content</label>
      <textarea name="content" class="form-control" id="content" placeholder="Content"></textarea>
  </div>

  <div class="form-group">
      <label for="image">Image</label>
      <input type="text" name="image" class="form-control" id="image" placeholder="Image">
  </div>
  
  <div class="form-group">
    <label for="category">Category</label>
    <select class="form-control" id="category" name="category_id">
        @foreach ($categories as $category)
            <option value="{{ $category->id }}">{{ $category->title }}</option>
        @endforeach           
    </select>
  </div>

  <div class="form-group">
    <label for="tags">Tags</label>
    <select multiple class="form-control" id="tags" name="tags[]">
        @foreach ($tags as $tag)
           <option value="{{ $tag->id }}">{{ $tag->title }}</option>  
        @endforeach           
    </select>
  </div>

  <button type="submit" class="btn btn-primary">Create</button>
</form>
    </div>
</div>

@endsection
```
# STEP 117 ADD $tags = Tag::all() IN public function create() AND 'tags' => '' IN public function store() IN MyPostController.php
```
public function create()
{
    $categories = Category::all();
    $tags = Tag::all();
    return view('post.create', compact('categories', 'tags'));
}

public function store()
    {
        $data = request()->validate([
            'title'       => 'string',
            'content'     => 'string',
            'image'       => 'string',
            'category_id' => '',
            'tags'      => ''      
        ]);
        $tags = $data['tags'];
        unset($data['tags']);        
        $post = Post::create($data);
        return redirect()->route('mypost.index');
    }

```
# STEP 118 ADD protected $guarded = false IN Models/PostTag.php
```
<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PostTag extends Model
{
    use HasFactory;
    protected $guarded = false;
}
```
# STEP 119 ADD FIRST METHOD IN public function store() IN MyPostController.php
```
 //FIRST METHOD 
        foreach ($tags as $tag) {
            PostTag::firstOrCreate([
                'tag_id' => $tag,
                'post_id' => $post->id
            ]);
        }
```
```
public function store()
    {
        $data = request()->validate([
            'title'       => 'string',
            'content'     => 'string',
            'image'       => 'string',
            'category_id' => '',
            'tags'      => ''      
        ]);
        $tags = $data['tags'];
        unset($data['tags']);               
        $post = Post::create($data);
        //FIRST METHOD 
        foreach ($tags as $tag) {
            PostTag::firstOrCreate([
                'tag_id' => $tag,
                'post_id' => $post->id
            ]);
        }
        return redirect()->route('mypost.index');
    }
```
# STEP 120 FINISHED LESSON 21 ADD SECOND METHOD IN public function store() IN MyPostController.php
```
//SECOND METHOD
$post->tags()->attach($tags);
```
```
public function store()
    {
        $data = request()->validate([
            'title'       => 'string',
            'content'     => 'string',
            'image'       => 'string',
            'category_id' => '',
            'tags'      => ''      
        ]);
        $tags = $data['tags'];
        unset($data['tags']);               
        $post = Post::create($data);
        //FIRST METHOD 
        // foreach ($tags as $tag) {
        //     PostTag::firstOrCreate([
        //         'tag_id' => $tag,
        //         'post_id' => $post->id
        //     ]);
        // }
        //SECOND METHOD
        $post->tags()->attach($tags);
        
        return redirect()->route('mypost.index');
    }
```
# STEP 121 ADD $tags = Tag::all() IN public function edit() IN MyPostController.php
```
public function edit(Post $post)
{
    $categories = Category::all();
    $tags = Tag::all();
    return view('post.edit', compact('post', 'categories', 'tags'));
}
```
# STEP 122 ADD Tags IN /resources/views/post/edit.blade.php
```
<div class="form-group">
    <label for="tags">Tags</label>
    <select multiple class="form-control" id="tags" name="tags[]">
        @foreach ($tags as $tag)
           <option 
           @foreach ($post->tags as $postTag)
            {{ $tag->id === $postTag->id ? ' selected' : ''}}
           @endforeach
           value="{{ $tag->id }}">{{ $tag->title }}</option>  
        @endforeach           
    </select>
</div>

```

```
@extends('layouts.myposts')
@section('myposts')

<div class="container">
    <div class="row">
        <h1 class="alert alert-warning">Update Posts</h1>
<form action="{{ route('mypost.update', $post->id) }}" method="POST">
    @csrf
    @method('patch')
  <div class="form-group">
      <label for="title">Title</label>
      <input type="text" name="title" class="form-control" id="title" placeholder="Title" value="{{ $post->title }}">
  </div>

  <div class="form-group">
      <label for="content">Content</label>
      <textarea name="content" class="form-control" id="content" placeholder="Content">{{ $post->content }}</textarea>
  </div>

  <div class="form-group">
      <label for="image">Image</label>
      <input type="text" name="image" class="form-control" id="image" placeholder="Image" value="{{ $post->image }}">
  </div>
  
  <div class="form-group">
    <label for="category">Category</label>
    <select class="form-control" id="category" name="category_id">
        @foreach ($categories as $category)
            <option
            {{ $category->id === $post->category->id ? ' selected' : ''}}
             value="{{ $category->id }}">{{ $category->title }}</option>
        @endforeach           
    </select>
  </div>

  <div class="form-group">
    <label for="tags">Tags</label>
    <select multiple class="form-control" id="tags" name="tags[]">
        @foreach ($tags as $tag)
           <option 
           @foreach ($post->tags as $postTag)
            {{ $tag->id === $postTag->id ? ' selected' : ''}}
           @endforeach
           value="{{ $tag->id }}">{{ $tag->title }}</option>  
        @endforeach           
    </select>
  </div>

  <button type="submit" class="btn btn-primary">Update</button>
</form>
    </div>
</div>

@endsection

```
# STEP 123 FINISHED LESSON 22 CHANGE public function update() IN MyPostController.php
```
public function update(Post $post)
    {
        $data = request()->validate([
            'title'       => 'string',
            'content'     => 'string',
            'image'       => 'string',
            'category_id' => '',
            'tags'        => ''
        ]);
        $tags = $data['tags'];
        unset($data['tags']);

        $post->update($data);
        $post->tags()->sync($tags);
        return redirect()->route('mypost.show', $post->id);
    }
```
# STEP 124 START LESSON 23 ADD @error, {{ old('title') }} IN /resources/views/post/create.blade.php
```
@error('title')
    <p class="text-danger">{{ $message }}</p>
@enderror

value="{{ old('title') }}

{{ old('category_id') == $category->id ? ' selected' : '' }}
```

```
@extends('layouts.myposts')
@section('myposts')

<div class="container">
    <div class="row">
        <h1 class="alert alert-warning">Create Posts</h1>
<form action="{{ route('mypost.store') }}" method="POST">
    @csrf
  <div class="form-group">
      <label for="title">Title</label>
      <input type="text" name="title" class="form-control" id="title" placeholder="Title" value="{{ old('title') }}" >
      @error('title')
          <p class="text-danger">{{ $message }}</p>
      @enderror
  </div>

  <div class="form-group">
      <label for="content">Content</label>
      <textarea name="content" class="form-control" id="content" placeholder="Content">{{ old('content') }}</textarea>
      @error('content')
        <p class="text-danger">{{ $message }}</p>
      @enderror
  </div>

  <div class="form-group">
      <label for="image">Image</label>
      <input type="text" name="image" class="form-control" id="image" placeholder="Image" value="{{ old('image') }}">
      @error('image')
          <p class="text-danger">{{ $message }}</p>
      @enderror
  </div>
  
  <div class="form-group">
    <label for="category">Category</label>
    <select class="form-control" id="category" name="category_id">
        @foreach ($categories as $category)
            <option 
            {{ old('category_id') == $category->id ? ' selected' : '' }}
            value="{{ $category->id }}">{{ $category->title }}</option>
        @endforeach           
    </select>
  </div>

  <div class="form-group">
    <label for="tags">Tags</label>
    <select multiple class="form-control" id="tags" name="tags[]">
        @foreach ($tags as $tag)
           <option value="{{ $tag->id }}">{{ $tag->title }}</option>  
        @endforeach           
    </select>
  </div>

  <button type="submit" class="btn btn-primary">Create</button>
</form>
    </div>
</div>

@endsection

```
# STEP 125 ADD 'title'=>'required|string' IN public function store() IN MyPostController.php
```
public function store()
    {
        $data = request()->validate([
            'title'       => 'required|string',
            'content'     => 'string',
            'image'       => 'string',
            'category_id' => '',
            'tags'      => ''      
        ]);
        $tags = $data['tags'];
        unset($data['tags']);               
        $post = Post::create($data);
        //FIRST METHOD 
        // foreach ($tags as $tag) {
        //     PostTag::firstOrCreate([
        //         'tag_id' => $tag,
        //         'post_id' => $post->id
        //     ]);
        // }
        //SECOND METHOD
        $post->tags()->attach($tags);

        return redirect()->route('mypost.index');
    }
```
# STEP 126 START LESSON 24 CREATE MODEL PostTwo WITH MIGRATION
```php artisan make:model Post2 -m```

# STEP 127 ADD CODE IN database/migrations/create_post2s_table.php
```
<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('post2s', function (Blueprint $table) {
            $table->id();
            $table->string('title');
            $table->text('content');
            $table->string('image')->nullable();
            $table->unsignedBigInteger('likes')->nullable();
            $table->boolean('is_published')->default(1);
            $table->timestamps();
            $table->softDeletes();

            $table->unsignedBigInteger('category_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('post2s');
    }
};
```
# STEP 128 CREATE MODEL Category2 WITH migration
```php artisan make:model Category2 -m```

```
<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('category2s', function (Blueprint $table) {
            $table->id();
            $table->string('title');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('category2s');
    }
};
```
# STEP 129 CREATE MODEL Tag2 WITH MIGRATION
```php artisan make:model Tag2 -m```

```
<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tag2s', function (Blueprint $table) {
            $table->id();
            $table->string('title');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tag2s');
    }
};
```
# STEP 130 CREATE MIGRATION create_post2_tag2_table.php
```php artisan make:migration create_post2_tag2_table --create```

```
<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('post2_tag2', function (Blueprint $table) {
            $table->id();

            $table->unsignedBigInteger('post2_id');
            $table->unsignedBigInteger('tag2_id');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('post2_tag2');
    }
};
```
# STEP 131 EXECUTE ALL MIGRATIONS
```php artisan migrate```

# STEP 132  ADD MANUALLY DATA IN TABLE 'post2s' 
```
id  title    content         image      likes is_published  category_id
1   cats     cats content    cats.jpg     10      1               1
2   chu      chu content     dogs.jpg     20      1               2
3   puma     puma content    puma.jpg     30      1               1
4   holiday  holiday content holiday.jpg  40      1               2
```
# STEP 133 ADD MANUALLY DATA IN TABLE 'category2s' IN FIELD 'title'
```
id  title
1   cats
2   dogs
3   fishes
```
# STEP 134 ADD CONTENT MANUALLY IN TABLE tag2s
```
id   title
1    foto
2    food
3    sleep
```
# STEP 135 ADD MANUALLY TAGS IN TABLE post_tags
```
id  post_id   tag_id    created_at    updated_at
1      1         1
2      1         3
3      2         2
4      2         1
5      3         1
6      3         1
7      3         1
``` 
# STEP 136 CREATE MyPostController2.php 
```php artisan make:controller MyPostController2```

# STEP 137 ADD CODE IN MyPostController2.php
```
<?php

namespace App\Http\Controllers;

use App\Models\Post2;
use Illuminate\Http\Request;

class MyPostController2 extends Controller
{
    public function index()
    {
        $posts = Post2::all();
        $post = Post2::find(1);
        dump($posts);
        dump($post->category2);
    }
}
```
# STEP 138 ADD ROUTE 'sleep' LOOK AT http://localhost:81/sleep
```
// Lesson 24
Route::get('sleep', 'SleepController@index')->name('sleep.index');
```
# STEP 139 FIXES ERROR 'Does not exists SleepController::index'
```
 1. composer update 
 2. composer dumpautoload
 3. php artisan config:cache
 4. php artisan view:clear
```
# STEP 140 ADD public function category2() IN Models/Post2.php
```
<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Post2 extends Model
{
    use HasFactory;

    public function category2()
    {
        return $this->belongsTo(Category2::class);
    }
}
```
# STEP 141 ADD public function post2s() IN Models/Category2.php
```
<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Category2 extends Model
{
    use HasFactory;

    public function post2s()
    {
        return $this->hasMany(Post2::class);
    }
}
```
# STEP 142 ADD $category = Category2::find(1); IN public function index() IN SleepController.php LOOK AT: http://localhost:81/sleep
```
<?php

namespace App\Http\Controllers;

use App\Models\Category2;
use App\Models\Post2;
use Illuminate\Http\Request;

class SleepController extends Controller
{
    public function index()
    {
        $posts = Post2::all();
        $post = Post2::find(1);
        $category = Category2::find(1);
        dump($posts);
        dump($post->category2);
        dump($category->post2s);
    }
    
}
```
# STEP 143 ADD CODE IN public function post2s() IN Models/Tag2.php
```
<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Tag2 extends Model
{
    use HasFactory;

    public function post2s()
    {
        return $this->BelongsToMany(Post2::class);
    }
}
```
# STEP 144 ADD $tag = Tag2::find(1); IN public function index() IN SleepController.php LOOK AT: http://localhost:81/sleep
```
<?php

namespace App\Http\Controllers;

use App\Models\Category2;
use App\Models\Post2;
use App\Models\Tag2;
use Illuminate\Http\Request;

class SleepController extends Controller
{
    public function index()
    {
        $posts = Post2::all();
        $post = Post2::find(1);
        $category = Category2::find(1);
        $tag = Tag2::find(1);
        dump($posts);
        dump($post->category2);
        dump($category->post2s);
        dump($tag->post2s);
    }
    
}
```
# STEP 145 ADD public function tag2s() IN Models/Post2.php
```
<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Post2 extends Model
{
    use HasFactory;

    public function category2()
    {
        return $this->belongsTo(Category2::class);
    }

    public function tag2s()
    {
        return $this->belongsToMany(Tag2::class);
    }
}
```
# STEP 146 FINISHED LESSON 24 ADD dump($post->tag2s); IN public function index() IN SleepController.php LOOK AT: http://localhost:81/sleep
```
<?php

namespace App\Http\Controllers;

use App\Models\Category2;
use App\Models\Post2;
use App\Models\Tag2;
use Illuminate\Http\Request;

class SleepController extends Controller
{
    public function index()
    {
        $posts = Post2::all();
        $post = Post2::find(1);
        $category = Category2::find(1);
        $tag = Tag2::find(1);
        dump($posts);
        dump($post->category2);
        dump($category->post2s);
        dump($tag->post2s);
        dump($post->tag2s);
    }
    
}
```
# STEP 147 START LESSON 25 ADD protected $guarded = false; IN ALL MODELS
```protected $guarded = false;```

# STEP 148 CREATE FOLDER Controller/Post AND CONTROLLER IndexController.php
```php artisan make:controller Post/IndexController```

# STEP 149 ADD public function __invoke() IN Controller/Post/IndexController.php
```
<?php

namespace App\Http\Controllers\Post;

use App\Http\Controllers\Controller;
use App\Models\Post;
use Illuminate\Http\Request;

class IndexController extends Controller
{
    public function __invoke()
    {
        $posts = Post::all();
        return view('post.index', compact('posts'));
    }
}
```
# STEP 150 ADD CONTROLLERS IN Controller/Post
```php artisan make:controller Post/CreateController```

```php artisan make:controller Post/StoreController```

```php artisan make:controller Post/ShowController```

```php artisan make:controller Post/DestroyController```

```php artisan make:controller Post/EditController```

```php artisan make:controller Post/UpdateController```

# STEP 151 ADD public function __invoke() IN ALL CONTROLLERS IN FOLDER Post AND COPY PAST INTERNAL FROM MyPostController.php

# STEP 152 FINISHED LESSON 25 COMMENT LESSON 17 IN routes/web.php COPY AND PAST ALL ROUTES IN LESSON 25 WITH NAMESPACE
```
// Lesson 25
Route::group(['namespace' => 'Post'], function ()
{
    Route::get('myposts', 'IndexController')->name('mypost.index');
    Route::get('myposts/create', 'CreateController')
    ->name('mypost.create');
    Route::post('myposts', 'StoreController')->name('mypost.store');
    Route::get('myposts/{post}', 'ShowController')
    ->name('mypost.show');
    Route::get('myposts/{post}/edit', 'EditController')
    ->name('mypost.edit');
    Route::patch('myposts/{post}', 'UpdateController')
    ->name('mypost.update');
    Route::delete('myposts/{post}', 'DestroyController')
    ->name('mypost.delete');

});
```
# STEP 153 START LESSON 26 CREATE Post/StoreRequest AND Post/UpdateRequest
```php artisan make:request Post/StoreRequest```

```php artisan make:request Post/UpdateRequest```

# STEP 154 CHANGE $data = $request->validated(); Controllers/Post/StoreController.php 
```
<?php

namespace App\Http\Controllers\Post;

use App\Http\Controllers\Controller;
use App\Http\Requests\Post\StoreRequest;
use App\Models\Post;
use Illuminate\Http\Request;

class StoreController extends Controller
{
    public function __invoke(StoreRequest $request)
    {
        $data = $request->validated();
        $tags = $data['tags'];
        unset($data['tags']);
        $post = Post::create($data);

        $post->tags()->attach($tags);

        return redirect()->route('mypost.index');
    }
}
```
# STEP 155 CHANGE $data = $request->validated(); Controllers/Post/UpdateController.php 
```
<?php

namespace App\Http\Controllers\Post;

use App\Http\Controllers\Controller;
use App\Http\Requests\Post\UpdateRequest;
use App\Models\Post;
use Illuminate\Http\Request;

class UpdateController extends Controller
{
    public function __invoke(UpdateRequest $request,Post $post)
    {
        $data = $request->validated();
        $tags = $data['tags'];
        unset($data['tags']);

        $post->update($data);
        $post->tags()->sync($tags);
        return redirect()->route('mypost.show', $post->id);
    }
}
```
# STEP 156 ADD RULE IN StoreRequest.php
```
<?php

namespace App\Http\Requests\Post;

use Illuminate\Foundation\Http\FormRequest;

class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title'       => 'required|string',
            'content'     => 'string',
            'image'       => 'string',
            'category_id' => '',
            'tags'      => ''
        ];
    }
}
```
# STEP 157 ADD RULE IN UpdateRequest.php FINISHED LESSON 26
```
<?php

namespace App\Http\Requests\Post;

use Illuminate\Foundation\Http\FormRequest;

class UpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title'       => 'string',
            'content'     => 'string',
            'image'       => 'string',
            'category_id' => '',
            'tags'        => ''
        ];
    }
}
```
# STEP 158 START LESSSON 27 CREATE App/Services/Post/Service.php
```
<?php 

namespace App\Services\Post;

class Service
{
  
}
```
# STEP 159 CREATE Controllers/BaseController.php
```php artisan make:controller Post/BaseController```

```
<?php

namespace App\Http\Controllers\Post;

use App\Http\Controllers\Controller;
use App\Services\Post\Service;
use Illuminate\Http\Request;

class BaseController extends Controller
{
    public $service;

    public function __construct(Service $service)
    {
        $this->service = $service;
    }
}
```
# STEP 160 CHANGE extends Controller ON extends BaseController IN ALL CONTROLLERS IN FOLDER Controllers/Post

# STEP 161 DELETE DATA FROM Controllers/Post/StoreController.php AND PAST IN App/Services/Service.php IN public function store($data)
```
$tags = $data['tags'];
unset($data['tags']);

$post = Post::create($data);
$post->tags()->attach($tags);
```

```
<?php 

namespace App\Services\Post;

use App\Models\Post;

class Service
{
    public function store($data)
    {
        $tags = $data['tags'];
        unset($data['tags']);

        $post = Post::create($data);
        $post->tags()->attach($tags);
    }

    public function update()
    {
     
    }
}
```


# STEP 162 PAST $this->service->store($data); in Controllers/Post/StoreController.php
```
<?php

namespace App\Http\Controllers\Post;

use App\Http\Controllers\Controller;
use App\Http\Requests\Post\StoreRequest;
use App\Models\Post;
use Illuminate\Http\Request;

class StoreController extends BaseController
{
    public function __invoke(StoreRequest $request)
    {
        $data = $request->validated();        

        $this->service->store($data);        

        return redirect()->route('mypost.index');
    }
}
```
# STEP 163  DELETE DATA FROM Controllers/Post/UpdateController.php AND PAST IN App/Services/Service.php IN public function update($post, $data)
```
<?php 

namespace App\Services\Post;

use App\Models\Post;

class Service
{
    public function store($data)
    {
        $tags = $data['tags'];
        unset($data['tags']);

        $post = Post::create($data);
        $post->tags()->attach($tags);
    }

    public function update($post, $data)
    {
        $tags = $data['tags'];
        unset($data['tags']);

        $post->update($data);
        $post->tags()->sync($tags);
    }
}
```
# STEP 164 PAST $this->service->update($post, $data); in Controllers/Post/UpdateController.php FINISHED LESSON 27
```
<?php

namespace App\Http\Controllers\Post;

use App\Http\Controllers\Controller;
use App\Http\Requests\Post\UpdateRequest;
use App\Models\Post;
use Illuminate\Http\Request;

class UpdateController extends BaseController
{
    public function __invoke(UpdateRequest $request,Post $post)
    {
        $data = $request->validated();
        
        $this->service->update($post, $data);

        return redirect()->route('mypost.show', $post->id);
    }
}
```
# STEP 165 START LESSON 28 CREATE FACTORY PostFactory.php
```php artisan make:factory PostFactory -m Post```
# STEP 166 ADD TEST CODE IN PostFactory IN public function definition()
```
<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Post>
 */
class PostFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'name' => 'Vasya',
            'surname' => 'Pupkin'
        ];
    }
}
```
# STEP 167 ADD TEST CODE IN DatabaseSeeder.php IN public function run()
```
<?php

namespace Database\Seeders;

use App\Models\Post;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $posts = Post::factory(10)->make();
        dd($posts);
        // \App\Models\User::factory(10)->create();
    }
}
```
# STEP 168 SHOW TEST CODE IN TERMINAL
```php artisan migrate --seed```
# STEP 169 CHANGE TEST CODE IN PostFactory.php IN public function definition()
```
<?php

namespace Database\Factories;

use App\Models\Category;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Post>
 */
class PostFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'title' => $this->faker->name(),
            'content' => $this->faker->text(),
            'image' => $this->faker->imageUrl(),
            'likes' => random_int(1 , 2000),
            'is_published' => 1,
            'category_id' => Category::get()->random()->id
        ];
    }
}
```
# STEP 170 CHANGE public function run() IN DatabaseSeeder.php
```
<?php

namespace Database\Seeders;

use App\Models\Post;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        Post::factory(100)->create();
        
        // \App\Models\User::factory(10)->create();
    }
}
```
# STEP 171 EXECUTE THIS CODE AND LOOK AT http://localhost:81/myposts
```php artisan migrate --seed```
# STEP 172 CREATE CategoryFactory.php AND ADD CODE
```php artisan make:factory CategoryFactory -m Category```

```
<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Category>
 */
class CategoryFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'title' => $this->faker->word()
        ];
    }
}

```
# STEP 173 CREATE TagFactory.php AND ADD CODE
```php artisan make:factory TagFactory -m Tag```

```
<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Tag>
 */
class TagFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'title' => $this->faker->word()
        ];
    }
}
```
# STEP 174 ADD NEW CODE IN DatabaseSeeder.php IN public function run()
```
<?php

namespace Database\Seeders;

use App\Models\Category;
use App\Models\Post;
use App\Models\Tag;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        Category::factory(20)->create();
        $tags = Tag::factory(50)->create();
        $posts = Post::factory(100)->create();
        
        foreach ($posts as $post) {
            $tagsIds = $tags->random(5)->pluck('id');
            $post->tags()->attach($tagsIds);
        }
        
        // \App\Models\User::factory(10)->create();
    }
}

```
# STEP 175 FINISHED LESSON 28 REFRESH ALL MIGRATES WITH SEED
```php artisan migrate:fresh --seed```
# STEP 176 START LESSON 29 CHANGE public function __invoke() IN Controllers/Post/IndexController.php $posts = Post::all() ON $posts = Post::paginate(10);
```
<?php

namespace App\Http\Controllers\Post;

use App\Http\Controllers\Controller;
use App\Models\Post;
use Illuminate\Http\Request;

class IndexController extends BaseController
{
    public function __invoke()
    {
        $posts = Post::paginate(10);
        return view('post.index', compact('posts'));
    }
}
```
# STEP 177 ADD {{ $posts->links() }} AT THE END OF /resources/views/post/index.blade.php
```
@extends('layouts.myposts')
@section('myposts')
    <div>
        <div>           
                <a href="{{ route('mypost.create') }}" 
                   class="btn btn-primary mb-3">
                    Create Post
                </a>                       
        </div>
        @foreach ($posts as $post)
            <div style="background: rgb(63, 78, 31)">
            <a href="{{ route('mypost.show', $post->id) }}">
              <b>{{ $post->id }}</b> . {{ $post->title }}
            </a> 
            </div>
        @endforeach

        <div class="mt-3">
            {{ $posts->links() }}
        </div>
    </div>
@endsection
```
# STEP 179 EXECUTE THIS COMMAND FOR PAGINATION
```php artisan vendor:publish --tag=laravel-pagination```
# STEP 180 FINISHED LESSON 29 ADD THIS CODE Paginator::defaultView('vendor.pagination.bootstrap-4'); IN app/Providers/AppServiceProvider.php IN public function boot()
```
<?php

namespace App\Providers;

use Illuminate\Pagination\Paginator;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Paginator::defaultView('vendor.pagination.bootstrap-4');
    }
}
```
# STEP 181 START LESSON 30 COMMENTS ALL IN public function __invoke() IN app/Http/Controllers/Post/IndexController.php
```
<?php

namespace App\Http\Controllers\Post;

use App\Http\Controllers\Controller;
use App\Models\Post;
use Illuminate\Http\Request;

class IndexController extends BaseController
{
    public function __invoke()
    {
        // $posts = Post::paginate(10);
        // return view('post.index', compact('posts'));
    }
}
```
# STEP 182 ADD TEST CODE IN public function __invoke() IN app/Http/Controllers/Post/IndexController.php
```
<?php

namespace App\Http\Controllers\Post;

use App\Http\Controllers\Controller;
use App\Models\Post;
use Illuminate\Http\Request;

class IndexController extends BaseController
{
    public function __invoke()
    {
        $posts = Post::where('is_published', 1)
        ->where('category_id', 5)
        ->get();
        dd($posts);
        // $posts = Post::paginate(10);
        // return view('post.index', compact('posts'));
    }
}
```
# STEP 183 CREATE Post/FilterRequest.php AND ADD RULES MUST CHANGE public function authorize() { return true;} 
```php artisan make:request Post/FilterRequest```
```
<?php

namespace App\Http\Requests\Post;

use Illuminate\Foundation\Http\FormRequest;

class FilterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title'       => 'string',
            'content'     => 'string',
            'image'       => 'string',
            'category_id' => '',
            'tags'      => ''
        ];
    }
}
```
# STEP 184 ADD FIRST REALIZATION FILTER, ADD CODE IN public function __invoke(FilterRequest $request) LOOK AT: http://localhost:81/myposts?category_id=5
```
<?php

namespace App\Http\Controllers\Post;

use App\Http\Controllers\Controller;
use App\Http\Requests\Post\FilterRequest;
use App\Models\Post;
use Illuminate\Http\Request;

class IndexController extends BaseController
{
    public function __invoke(FilterRequest $request)
    {
        $data = $request->validated();        
        $query = Post::query();
        

        if (isset($data['category_id'])) {
            $query->where('category_id', $data['category_id']);
        }

        $posts = $query->get();
        dd($posts);
        // $posts = Post::paginate(10);
        // return view('post.index', compact('posts'));
    }
}
```
# STEP 185 ADD if(isset($data['title'])) AND if(isset($data['content'])) LOOK AT: http://localhost:81/myposts?title=ana SEARCH KEYWORD 'ana' http://localhost:81/myposts?content=vim SEARCH KEYWORD 'vim'  http://localhost:81/myposts?title=ana&category_id=17 SEARCH KEYWORD 'ana' AND category_id=17
```
<?php

namespace App\Http\Controllers\Post;

use App\Http\Controllers\Controller;
use App\Http\Requests\Post\FilterRequest;
use App\Models\Post;
use Illuminate\Http\Request;

class IndexController extends BaseController
{
    public function __invoke(FilterRequest $request)
    {
        $data = $request->validated();        
        $query = Post::query();
        

        if (isset($data['category_id'])) {
            $query->where('category_id', $data['category_id']);
        }
        if (isset($data['title'])) {
            $query->where('title', 'like', "%{$data['title']}%");
        }
        if (isset($data['content'])) {
            $query->where('content', 'like', "%{$data['content']}%");
        }

        $posts = $query->get();
        dd($posts);
        // $posts = Post::paginate(10);
        // return view('post.index', compact('posts'));
    }
}
```
# STEP 186 CREATE FOLDER Filters IN app/Http/ AND ADD INTO FILES: AbstractFilter.php FilterInterface.php PostFilter.php
# STEP 187 ADD public function apply(Builder $builder) IN app/Http/Filters/FilterInterface.php
```
<?php

namespace App\Http\Filters;

use Illuminate\Database\Eloquent\Builder;

interface FilterInterface
{
    public function apply(Builder $builder);
}
```
# STEP 188 FIILED CODE IN app/Http/Filters/AbstarctFilter.php
```
<?php

namespace App\Http\Filters;

use Illuminate\Database\Eloquent\Builder;

abstract class AbstractFilter implements FilterInterface
{
    /** @var array */
    private $queryParams = [];

    /**
     * AbstractFilter constructor.
     *
     * @param array $queryParams
     */
    public function __construct(array $queryParams)
    {
        $this->queryParams = $queryParams;
    }

    abstract protected function getCallbacks(): array;

    public function apply(Builder $builder)
    {
        $this->before($builder);

        foreach ($this->getCallbacks() as $name => $callback) {
            if (isset($this->queryParams[$name])) {
                call_user_func($callback, $builder, $this->queryParams[$name]);
            }
        }
    }

    /**
     * @param Builder $builder
     */
    protected function before(Builder $builder)
    {
    }

    /**
     * @param string $key
     * @param mixed|null $default
     *
     * @return mixed|null
     */
    protected function getQueryParam(string $key, $default = null)
    {
        return $this->queryParams[$key] ?? $default;
    }

    /**
     * @param string[] $keys
     *
     * @return AbstractFilter
     */
    protected function removeQueryParam(string ...$keys)
    {
        foreach ($keys as $key) {
            unset($this->queryParams[$key]);
        }

        return $this;
    }
}
```
# STEP 189 CREATE FOLDER Traits AND ADD INTO TRAIT Filterable.php
```
<?php

namespace App\Models\Traits;

use App\Http\Filters\FilterInterface;
use Illuminate\Database\Eloquent\Builder;

trait Filterable
{
    /**
     * @param Builder $builder
     * @param FilterInterface $filter
     *
     * @return Builder
     */
    public function scopeFilter(Builder $builder, FilterInterface $filter)
    {
        $filter->apply($builder);

        return $builder;
    }
}
```
# STEP 190 ADD PUBLIC CONSTANTS IN app//Http/Filters/PostFilter.php AND extends AbstractFilter.php
```
<?php

namespace App\Http\Filters;

class PostFilter
{
    public const TITLE = 'title';
    public const CONTENT = 'content';
    public const IMAGE = 'image';
    public const CATEGORY_ID = 'category_id';
    public const TAGS = 'tags';
}
```
# STEP 191 ADD 5 filtering public functions IN app//Http/Filters/PostFilter.php AND ABSTRACT METHOD getCallbacks(): array
```
<?php

namespace App\Http\Filters;

use Illuminate\Database\Eloquent\Builder;

class PostFilter extends AbstractFilter
{
    public const TITLE = 'title';
    public const CONTENT = 'content';
    public const IMAGE = 'image';
    public const CATEGORY_ID = 'category_id';
    public const TAGS = 'tags';

    protected function getCallbacks(): array
    {
        return [
            self::TITLE =>[$this, 'title'],
            self::CONTENT => [$this, 'content'],
            self::IMAGE => [$this, 'image'],
            self::CATEGORY_ID => [$this, 'categoryId'],
            self::TAGS => [$this, 'tags']
        ];
    }

    public function title(Builder $builder, $value)
    {
        $builder->where('title', 'like', "%{$value}%");
    }
    public function content(Builder $builder, $value)
    {
        $builder->where('content', 'like', "%{$value}%");
    }
    public function image(Builder $builder, $value)
    {
        $builder->where('image', 'like', "%{$value}%");
    }
    public function categoryId(Builder $builder, $value)
    {
        $builder->where('category_id', 'like', $value);
    }
    public function tags(Builder $builder, $value)
    {
        $builder->where('tags', 'like', $value);
    }
}
```
# STEP 192 ADD TRAIT Filterable IN app\Models\Post.php
```
<?php

namespace App\Models;

use App\Models\Traits\Filterable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Post extends Model
{
    use HasFactory;
    use SoftDeletes;
    use  Filterable;
    protected $table = 'posts';
    protected $guarded = false;
    //protected $guarded = [];

    public function category()
    {
        return $this->belongsTo(Category::class, 'category_id', 'id');
    }

    public function tags()
    {
        return $this->belongsToMany(Tag::class, 'post_tags', 'post_id', 'tag_id');
    }
}
```
# STEP 193 DELETE PARTS OF CODE IN public function __invoke(FilterRequest $request) AND CREATE NEW REALIZATION IN app\Http\Controllers\Post\IndexController.php
### DELETED CODE
```
$query = Post::query();
        

        if (isset($data['category_id'])) {
            $query->where('category_id', $data['category_id']);
        }
        if (isset($data['title'])) {
            $query->where('title', 'like', "%{$data['title']}%");
        }
        if (isset($data['content'])) {
            $query->where('content', 'like', "%{$data['content']}%");
        }

        $posts = $query->get();
```
### ADDED CODE
```
 $filter = app()
           ->make(PostFilter::class, 
           ['queryParams'=>array_filter($data)]);

$posts = Post::filter($filter)->get();       

```

```
<?php

namespace App\Http\Controllers\Post;

use App\Http\Controllers\Controller;
use App\Http\Filters\PostFilter;
use App\Http\Requests\Post\FilterRequest;
use App\Models\Post;
use Illuminate\Http\Request;

class IndexController extends BaseController
{
    public function __invoke(FilterRequest $request)
    {
        $data = $request->validated(); 
        $filter = app()
                 ->make(PostFilter::class, 
                 ['queryParams'=>array_filter($data)]);
        $posts = Post::filter($filter)->get();
        
        dd($posts);
        // $posts = Post::paginate(10);
        // return view('post.index', compact('posts'));
    }
}
```
# STEP 194 CHANGE public function __invoke(FilterRequest $request) IN app\Http\Controllers\Post\IndexController.php LOOK AT: http://localhost:81/myposts?title=ana&content=pla
```
<?php

namespace App\Http\Controllers\Post;

use App\Http\Controllers\Controller;
use App\Http\Filters\PostFilter;
use App\Http\Requests\Post\FilterRequest;
use App\Models\Post;
use Illuminate\Http\Request;

class IndexController extends BaseController
{
    public function __invoke(FilterRequest $request)
    {
        $data = $request->validated(); 
        $filter = app()
                 ->make(PostFilter::class, 
                 ['queryParams'=>array_filter($data)]);
        $posts = Post::filter($filter)->paginate(10);        
        
        return view('post.index', compact('posts'));
    }
}
```
# STEP 195 FINISHED LESSON 30 ADD $posts->withQueryString()->links() IN resources\views\post\index.blade.php
```
@extends('layouts.myposts')
@section('myposts')
    <div>
        <div>           
                <a href="{{ route('mypost.create') }}" 
                   class="btn btn-primary mb-3">
                    Create Post
                </a>                       
        </div>
        @foreach ($posts as $post)
            <div style="background: rgb(63, 78, 31)">
            <a href="{{ route('mypost.show', $post->id) }}">
              <b>{{ $post->id }}</b> . {{ $post->title }}
            </a> 
            </div>
        @endforeach

        <div class="mt-3">
            {{ $posts->withQueryString()->links() }}
        </div>
    </div>
@endsection
```
# STEP 196 START LESSON 31 SWITCH ON /resources/views/main.blade.php COMMENT AND ADD CODE LOOK AT: http://localhost:81/main
```
{{-- @extends('layouts.main')
@section('content')
        <h1 style="background: rgb(128, 51, 0)">
            This is main page
        </h1>
@endsection --}}

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>AdminLTE 3 | Dashboard</title>

  <!-- Google Font: Source Sans Pro -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="plugins/fontawesome-free/css/all.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Tempusdominus Bootstrap 4 -->
  <link rel="stylesheet" href="plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="plugins/icheck-bootstrap/icheck-bootstrap.min.css">
  <!-- JQVMap -->
  <link rel="stylesheet" href="plugins/jqvmap/jqvmap.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="dist/css/adminlte.min.css">
  <!-- overlayScrollbars -->
  <link rel="stylesheet" href="plugins/overlayScrollbars/css/OverlayScrollbars.min.css">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="plugins/daterangepicker/daterangepicker.css">
  <!-- summernote -->
  <link rel="stylesheet" href="plugins/summernote/summernote-bs4.min.css">
</head>
<body class="hold-transition sidebar-mini layout-fixed">
<div class="wrapper">

  <!-- Preloader -->
  <div class="preloader flex-column justify-content-center align-items-center">
    <img class="animation__shake" src="dist/img/AdminLTELogo.png" alt="AdminLTELogo" height="60" width="60">
  </div>

  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand navbar-white navbar-light">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
      </li>
      <li class="nav-item d-none d-sm-inline-block">
        <a href="index3.html" class="nav-link">Home</a>
      </li>
      <li class="nav-item d-none d-sm-inline-block">
        <a href="#" class="nav-link">Contact</a>
      </li>
    </ul>

    <!-- Right navbar links -->
    <ul class="navbar-nav ml-auto">
      <!-- Navbar Search -->
      <li class="nav-item">
        <a class="nav-link" data-widget="navbar-search" href="#" role="button">
          <i class="fas fa-search"></i>
        </a>
        <div class="navbar-search-block">
          <form class="form-inline">
            <div class="input-group input-group-sm">
              <input class="form-control form-control-navbar" type="search" placeholder="Search" aria-label="Search">
              <div class="input-group-append">
                <button class="btn btn-navbar" type="submit">
                  <i class="fas fa-search"></i>
                </button>
                <button class="btn btn-navbar" type="button" data-widget="navbar-search">
                  <i class="fas fa-times"></i>
                </button>
              </div>
            </div>
          </form>
        </div>
      </li>

      <!-- Messages Dropdown Menu -->
      <li class="nav-item dropdown">
        <a class="nav-link" data-toggle="dropdown" href="#">
          <i class="far fa-comments"></i>
          <span class="badge badge-danger navbar-badge">3</span>
        </a>
        <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
          <a href="#" class="dropdown-item">
            <!-- Message Start -->
            <div class="media">
              <img src="dist/img/user1-128x128.jpg" alt="User Avatar" class="img-size-50 mr-3 img-circle">
              <div class="media-body">
                <h3 class="dropdown-item-title">
                  Brad Diesel
                  <span class="float-right text-sm text-danger"><i class="fas fa-star"></i></span>
                </h3>
                <p class="text-sm">Call me whenever you can...</p>
                <p class="text-sm text-muted"><i class="far fa-clock mr-1"></i> 4 Hours Ago</p>
              </div>
            </div>
            <!-- Message End -->
          </a>
          <div class="dropdown-divider"></div>
          <a href="#" class="dropdown-item">
            <!-- Message Start -->
            <div class="media">
              <img src="dist/img/user8-128x128.jpg" alt="User Avatar" class="img-size-50 img-circle mr-3">
              <div class="media-body">
                <h3 class="dropdown-item-title">
                  John Pierce
                  <span class="float-right text-sm text-muted"><i class="fas fa-star"></i></span>
                </h3>
                <p class="text-sm">I got your message bro</p>
                <p class="text-sm text-muted"><i class="far fa-clock mr-1"></i> 4 Hours Ago</p>
              </div>
            </div>
            <!-- Message End -->
          </a>
          <div class="dropdown-divider"></div>
          <a href="#" class="dropdown-item">
            <!-- Message Start -->
            <div class="media">
              <img src="dist/img/user3-128x128.jpg" alt="User Avatar" class="img-size-50 img-circle mr-3">
              <div class="media-body">
                <h3 class="dropdown-item-title">
                  Nora Silvester
                  <span class="float-right text-sm text-warning"><i class="fas fa-star"></i></span>
                </h3>
                <p class="text-sm">The subject goes here</p>
                <p class="text-sm text-muted"><i class="far fa-clock mr-1"></i> 4 Hours Ago</p>
              </div>
            </div>
            <!-- Message End -->
          </a>
          <div class="dropdown-divider"></div>
          <a href="#" class="dropdown-item dropdown-footer">See All Messages</a>
        </div>
      </li>
      <!-- Notifications Dropdown Menu -->
      <li class="nav-item dropdown">
        <a class="nav-link" data-toggle="dropdown" href="#">
          <i class="far fa-bell"></i>
          <span class="badge badge-warning navbar-badge">15</span>
        </a>
        <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
          <span class="dropdown-item dropdown-header">15 Notifications</span>
          <div class="dropdown-divider"></div>
          <a href="#" class="dropdown-item">
            <i class="fas fa-envelope mr-2"></i> 4 new messages
            <span class="float-right text-muted text-sm">3 mins</span>
          </a>
          <div class="dropdown-divider"></div>
          <a href="#" class="dropdown-item">
            <i class="fas fa-users mr-2"></i> 8 friend requests
            <span class="float-right text-muted text-sm">12 hours</span>
          </a>
          <div class="dropdown-divider"></div>
          <a href="#" class="dropdown-item">
            <i class="fas fa-file mr-2"></i> 3 new reports
            <span class="float-right text-muted text-sm">2 days</span>
          </a>
          <div class="dropdown-divider"></div>
          <a href="#" class="dropdown-item dropdown-footer">See All Notifications</a>
        </div>
      </li>
      <li class="nav-item">
        <a class="nav-link" data-widget="fullscreen" href="#" role="button">
          <i class="fas fa-expand-arrows-alt"></i>
        </a>
      </li>
      <li class="nav-item">
        <a class="nav-link" data-widget="control-sidebar" data-controlsidebar-slide="true" href="#" role="button">
          <i class="fas fa-th-large"></i>
        </a>
      </li>
    </ul>
  </nav>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="index3.html" class="brand-link">
      <img src="dist/img/AdminLTELogo.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
      <span class="brand-text font-weight-light">AdminLTE 3</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src="dist/img/user2-160x160.jpg" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
          <a href="#" class="d-block">Alexander Pierce</a>
        </div>
      </div>

      <!-- SidebarSearch Form -->
      <div class="form-inline">
        <div class="input-group" data-widget="sidebar-search">
          <input class="form-control form-control-sidebar" type="search" placeholder="Search" aria-label="Search">
          <div class="input-group-append">
            <button class="btn btn-sidebar">
              <i class="fas fa-search fa-fw"></i>
            </button>
          </div>
        </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          <li class="nav-item menu-open">
            <a href="#" class="nav-link active">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>
                Dashboard
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="./index.html" class="nav-link active">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Dashboard v1</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="./index2.html" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Dashboard v2</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="./index3.html" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Dashboard v3</p>
                </a>
              </li>
            </ul>
          </li>
          <li class="nav-item">
            <a href="pages/widgets.html" class="nav-link">
              <i class="nav-icon fas fa-th"></i>
              <p>
                Widgets
                <span class="right badge badge-danger">New</span>
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-copy"></i>
              <p>
                Layout Options
                <i class="fas fa-angle-left right"></i>
                <span class="badge badge-info right">6</span>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="pages/layout/top-nav.html" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Top Navigation</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="pages/layout/top-nav-sidebar.html" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Top Navigation + Sidebar</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="pages/layout/boxed.html" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Boxed</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="pages/layout/fixed-sidebar.html" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Fixed Sidebar</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="pages/layout/fixed-sidebar-custom.html" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Fixed Sidebar <small>+ Custom Area</small></p>
                </a>
              </li>
              <li class="nav-item">
                <a href="pages/layout/fixed-topnav.html" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Fixed Navbar</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="pages/layout/fixed-footer.html" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Fixed Footer</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="pages/layout/collapsed-sidebar.html" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Collapsed Sidebar</p>
                </a>
              </li>
            </ul>
          </li>
          <li class="nav-item">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-chart-pie"></i>
              <p>
                Charts
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="pages/charts/chartjs.html" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>ChartJS</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="pages/charts/flot.html" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Flot</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="pages/charts/inline.html" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Inline</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="pages/charts/uplot.html" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>uPlot</p>
                </a>
              </li>
            </ul>
          </li>
          <li class="nav-item">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-tree"></i>
              <p>
                UI Elements
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="pages/UI/general.html" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>General</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="pages/UI/icons.html" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Icons</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="pages/UI/buttons.html" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Buttons</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="pages/UI/sliders.html" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Sliders</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="pages/UI/modals.html" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Modals & Alerts</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="pages/UI/navbar.html" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Navbar & Tabs</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="pages/UI/timeline.html" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Timeline</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="pages/UI/ribbons.html" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Ribbons</p>
                </a>
              </li>
            </ul>
          </li>
          <li class="nav-item">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-edit"></i>
              <p>
                Forms
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="pages/forms/general.html" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>General Elements</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="pages/forms/advanced.html" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Advanced Elements</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="pages/forms/editors.html" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Editors</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="pages/forms/validation.html" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Validation</p>
                </a>
              </li>
            </ul>
          </li>
          <li class="nav-item">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-table"></i>
              <p>
                Tables
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="pages/tables/simple.html" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Simple Tables</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="pages/tables/data.html" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>DataTables</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="pages/tables/jsgrid.html" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>jsGrid</p>
                </a>
              </li>
            </ul>
          </li>
          <li class="nav-header">EXAMPLES</li>
          <li class="nav-item">
            <a href="pages/calendar.html" class="nav-link">
              <i class="nav-icon far fa-calendar-alt"></i>
              <p>
                Calendar
                <span class="badge badge-info right">2</span>
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="pages/gallery.html" class="nav-link">
              <i class="nav-icon far fa-image"></i>
              <p>
                Gallery
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="pages/kanban.html" class="nav-link">
              <i class="nav-icon fas fa-columns"></i>
              <p>
                Kanban Board
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="#" class="nav-link">
              <i class="nav-icon far fa-envelope"></i>
              <p>
                Mailbox
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="pages/mailbox/mailbox.html" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Inbox</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="pages/mailbox/compose.html" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Compose</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="pages/mailbox/read-mail.html" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Read</p>
                </a>
              </li>
            </ul>
          </li>
          <li class="nav-item">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-book"></i>
              <p>
                Pages
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="pages/examples/invoice.html" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Invoice</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="pages/examples/profile.html" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Profile</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="pages/examples/e-commerce.html" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>E-commerce</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="pages/examples/projects.html" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Projects</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="pages/examples/project-add.html" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Project Add</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="pages/examples/project-edit.html" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Project Edit</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="pages/examples/project-detail.html" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Project Detail</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="pages/examples/contacts.html" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Contacts</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="pages/examples/faq.html" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>FAQ</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="pages/examples/contact-us.html" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Contact us</p>
                </a>
              </li>
            </ul>
          </li>
          <li class="nav-item">
            <a href="#" class="nav-link">
              <i class="nav-icon far fa-plus-square"></i>
              <p>
                Extras
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="#" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>
                    Login & Register v1
                    <i class="fas fa-angle-left right"></i>
                  </p>
                </a>
                <ul class="nav nav-treeview">
                  <li class="nav-item">
                    <a href="pages/examples/login.html" class="nav-link">
                      <i class="far fa-circle nav-icon"></i>
                      <p>Login v1</p>
                    </a>
                  </li>
                  <li class="nav-item">
                    <a href="pages/examples/register.html" class="nav-link">
                      <i class="far fa-circle nav-icon"></i>
                      <p>Register v1</p>
                    </a>
                  </li>
                  <li class="nav-item">
                    <a href="pages/examples/forgot-password.html" class="nav-link">
                      <i class="far fa-circle nav-icon"></i>
                      <p>Forgot Password v1</p>
                    </a>
                  </li>
                  <li class="nav-item">
                    <a href="pages/examples/recover-password.html" class="nav-link">
                      <i class="far fa-circle nav-icon"></i>
                      <p>Recover Password v1</p>
                    </a>
                  </li>
                </ul>
              </li>
              <li class="nav-item">
                <a href="#" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>
                    Login & Register v2
                    <i class="fas fa-angle-left right"></i>
                  </p>
                </a>
                <ul class="nav nav-treeview">
                  <li class="nav-item">
                    <a href="pages/examples/login-v2.html" class="nav-link">
                      <i class="far fa-circle nav-icon"></i>
                      <p>Login v2</p>
                    </a>
                  </li>
                  <li class="nav-item">
                    <a href="pages/examples/register-v2.html" class="nav-link">
                      <i class="far fa-circle nav-icon"></i>
                      <p>Register v2</p>
                    </a>
                  </li>
                  <li class="nav-item">
                    <a href="pages/examples/forgot-password-v2.html" class="nav-link">
                      <i class="far fa-circle nav-icon"></i>
                      <p>Forgot Password v2</p>
                    </a>
                  </li>
                  <li class="nav-item">
                    <a href="pages/examples/recover-password-v2.html" class="nav-link">
                      <i class="far fa-circle nav-icon"></i>
                      <p>Recover Password v2</p>
                    </a>
                  </li>
                </ul>
              </li>
              <li class="nav-item">
                <a href="pages/examples/lockscreen.html" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Lockscreen</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="pages/examples/legacy-user-menu.html" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Legacy User Menu</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="pages/examples/language-menu.html" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Language Menu</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="pages/examples/404.html" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Error 404</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="pages/examples/500.html" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Error 500</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="pages/examples/pace.html" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Pace</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="pages/examples/blank.html" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Blank Page</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="starter.html" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Starter Page</p>
                </a>
              </li>
            </ul>
          </li>
          <li class="nav-item">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-search"></i>
              <p>
                Search
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="pages/search/simple.html" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Simple Search</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="pages/search/enhanced.html" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Enhanced</p>
                </a>
              </li>
            </ul>
          </li>
          <li class="nav-header">MISCELLANEOUS</li>
          <li class="nav-item">
            <a href="iframe.html" class="nav-link">
              <i class="nav-icon fas fa-ellipsis-h"></i>
              <p>Tabbed IFrame Plugin</p>
            </a>
          </li>
          <li class="nav-item">
            <a href="https://adminlte.io/docs/3.1/" class="nav-link">
              <i class="nav-icon fas fa-file"></i>
              <p>Documentation</p>
            </a>
          </li>
          <li class="nav-header">MULTI LEVEL EXAMPLE</li>
          <li class="nav-item">
            <a href="#" class="nav-link">
              <i class="fas fa-circle nav-icon"></i>
              <p>Level 1</p>
            </a>
          </li>
          <li class="nav-item">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-circle"></i>
              <p>
                Level 1
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="#" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Level 2</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="#" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>
                    Level 2
                    <i class="right fas fa-angle-left"></i>
                  </p>
                </a>
                <ul class="nav nav-treeview">
                  <li class="nav-item">
                    <a href="#" class="nav-link">
                      <i class="far fa-dot-circle nav-icon"></i>
                      <p>Level 3</p>
                    </a>
                  </li>
                  <li class="nav-item">
                    <a href="#" class="nav-link">
                      <i class="far fa-dot-circle nav-icon"></i>
                      <p>Level 3</p>
                    </a>
                  </li>
                  <li class="nav-item">
                    <a href="#" class="nav-link">
                      <i class="far fa-dot-circle nav-icon"></i>
                      <p>Level 3</p>
                    </a>
                  </li>
                </ul>
              </li>
              <li class="nav-item">
                <a href="#" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Level 2</p>
                </a>
              </li>
            </ul>
          </li>
          <li class="nav-item">
            <a href="#" class="nav-link">
              <i class="fas fa-circle nav-icon"></i>
              <p>Level 1</p>
            </a>
          </li>
          <li class="nav-header">LABELS</li>
          <li class="nav-item">
            <a href="#" class="nav-link">
              <i class="nav-icon far fa-circle text-danger"></i>
              <p class="text">Important</p>
            </a>
          </li>
          <li class="nav-item">
            <a href="#" class="nav-link">
              <i class="nav-icon far fa-circle text-warning"></i>
              <p>Warning</p>
            </a>
          </li>
          <li class="nav-item">
            <a href="#" class="nav-link">
              <i class="nav-icon far fa-circle text-info"></i>
              <p>Informational</p>
            </a>
          </li>
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Dashboard</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Dashboard v1</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- Small boxes (Stat box) -->
        <div class="row">
          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-info">
              <div class="inner">
                <h3>150</h3>

                <p>New Orders</p>
              </div>
              <div class="icon">
                <i class="ion ion-bag"></i>
              </div>
              <a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div>
          <!-- ./col -->
          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-success">
              <div class="inner">
                <h3>53<sup style="font-size: 20px">%</sup></h3>

                <p>Bounce Rate</p>
              </div>
              <div class="icon">
                <i class="ion ion-stats-bars"></i>
              </div>
              <a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div>
          <!-- ./col -->
          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-warning">
              <div class="inner">
                <h3>44</h3>

                <p>User Registrations</p>
              </div>
              <div class="icon">
                <i class="ion ion-person-add"></i>
              </div>
              <a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div>
          <!-- ./col -->
          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-danger">
              <div class="inner">
                <h3>65</h3>

                <p>Unique Visitors</p>
              </div>
              <div class="icon">
                <i class="ion ion-pie-graph"></i>
              </div>
              <a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div>
          <!-- ./col -->
        </div>
        <!-- /.row -->
        <!-- Main row -->
        <div class="row">
          <!-- Left col -->
          <section class="col-lg-7 connectedSortable">
            <!-- Custom tabs (Charts with tabs)-->
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">
                  <i class="fas fa-chart-pie mr-1"></i>
                  Sales
                </h3>
                <div class="card-tools">
                  <ul class="nav nav-pills ml-auto">
                    <li class="nav-item">
                      <a class="nav-link active" href="#revenue-chart" data-toggle="tab">Area</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" href="#sales-chart" data-toggle="tab">Donut</a>
                    </li>
                  </ul>
                </div>
              </div><!-- /.card-header -->
              <div class="card-body">
                <div class="tab-content p-0">
                  <!-- Morris chart - Sales -->
                  <div class="chart tab-pane active" id="revenue-chart"
                       style="position: relative; height: 300px;">
                      <canvas id="revenue-chart-canvas" height="300" style="height: 300px;"></canvas>
                   </div>
                  <div class="chart tab-pane" id="sales-chart" style="position: relative; height: 300px;">
                    <canvas id="sales-chart-canvas" height="300" style="height: 300px;"></canvas>
                  </div>
                </div>
              </div><!-- /.card-body -->
            </div>
            <!-- /.card -->

            <!-- DIRECT CHAT -->
            <div class="card direct-chat direct-chat-primary">
              <div class="card-header">
                <h3 class="card-title">Direct Chat</h3>

                <div class="card-tools">
                  <span title="3 New Messages" class="badge badge-primary">3</span>
                  <button type="button" class="btn btn-tool" data-card-widget="collapse">
                    <i class="fas fa-minus"></i>
                  </button>
                  <button type="button" class="btn btn-tool" title="Contacts" data-widget="chat-pane-toggle">
                    <i class="fas fa-comments"></i>
                  </button>
                  <button type="button" class="btn btn-tool" data-card-widget="remove">
                    <i class="fas fa-times"></i>
                  </button>
                </div>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <!-- Conversations are loaded here -->
                <div class="direct-chat-messages">
                  <!-- Message. Default to the left -->
                  <div class="direct-chat-msg">
                    <div class="direct-chat-infos clearfix">
                      <span class="direct-chat-name float-left">Alexander Pierce</span>
                      <span class="direct-chat-timestamp float-right">23 Jan 2:00 pm</span>
                    </div>
                    <!-- /.direct-chat-infos -->
                    <img class="direct-chat-img" src="dist/img/user1-128x128.jpg" alt="message user image">
                    <!-- /.direct-chat-img -->
                    <div class="direct-chat-text">
                      Is this template really for free? That's unbelievable!
                    </div>
                    <!-- /.direct-chat-text -->
                  </div>
                  <!-- /.direct-chat-msg -->

                  <!-- Message to the right -->
                  <div class="direct-chat-msg right">
                    <div class="direct-chat-infos clearfix">
                      <span class="direct-chat-name float-right">Sarah Bullock</span>
                      <span class="direct-chat-timestamp float-left">23 Jan 2:05 pm</span>
                    </div>
                    <!-- /.direct-chat-infos -->
                    <img class="direct-chat-img" src="dist/img/user3-128x128.jpg" alt="message user image">
                    <!-- /.direct-chat-img -->
                    <div class="direct-chat-text">
                      You better believe it!
                    </div>
                    <!-- /.direct-chat-text -->
                  </div>
                  <!-- /.direct-chat-msg -->

                  <!-- Message. Default to the left -->
                  <div class="direct-chat-msg">
                    <div class="direct-chat-infos clearfix">
                      <span class="direct-chat-name float-left">Alexander Pierce</span>
                      <span class="direct-chat-timestamp float-right">23 Jan 5:37 pm</span>
                    </div>
                    <!-- /.direct-chat-infos -->
                    <img class="direct-chat-img" src="dist/img/user1-128x128.jpg" alt="message user image">
                    <!-- /.direct-chat-img -->
                    <div class="direct-chat-text">
                      Working with AdminLTE on a great new app! Wanna join?
                    </div>
                    <!-- /.direct-chat-text -->
                  </div>
                  <!-- /.direct-chat-msg -->

                  <!-- Message to the right -->
                  <div class="direct-chat-msg right">
                    <div class="direct-chat-infos clearfix">
                      <span class="direct-chat-name float-right">Sarah Bullock</span>
                      <span class="direct-chat-timestamp float-left">23 Jan 6:10 pm</span>
                    </div>
                    <!-- /.direct-chat-infos -->
                    <img class="direct-chat-img" src="dist/img/user3-128x128.jpg" alt="message user image">
                    <!-- /.direct-chat-img -->
                    <div class="direct-chat-text">
                      I would love to.
                    </div>
                    <!-- /.direct-chat-text -->
                  </div>
                  <!-- /.direct-chat-msg -->

                </div>
                <!--/.direct-chat-messages-->

                <!-- Contacts are loaded here -->
                <div class="direct-chat-contacts">
                  <ul class="contacts-list">
                    <li>
                      <a href="#">
                        <img class="contacts-list-img" src="dist/img/user1-128x128.jpg" alt="User Avatar">

                        <div class="contacts-list-info">
                          <span class="contacts-list-name">
                            Count Dracula
                            <small class="contacts-list-date float-right">2/28/2015</small>
                          </span>
                          <span class="contacts-list-msg">How have you been? I was...</span>
                        </div>
                        <!-- /.contacts-list-info -->
                      </a>
                    </li>
                    <!-- End Contact Item -->
                    <li>
                      <a href="#">
                        <img class="contacts-list-img" src="dist/img/user7-128x128.jpg" alt="User Avatar">

                        <div class="contacts-list-info">
                          <span class="contacts-list-name">
                            Sarah Doe
                            <small class="contacts-list-date float-right">2/23/2015</small>
                          </span>
                          <span class="contacts-list-msg">I will be waiting for...</span>
                        </div>
                        <!-- /.contacts-list-info -->
                      </a>
                    </li>
                    <!-- End Contact Item -->
                    <li>
                      <a href="#">
                        <img class="contacts-list-img" src="dist/img/user3-128x128.jpg" alt="User Avatar">

                        <div class="contacts-list-info">
                          <span class="contacts-list-name">
                            Nadia Jolie
                            <small class="contacts-list-date float-right">2/20/2015</small>
                          </span>
                          <span class="contacts-list-msg">I'll call you back at...</span>
                        </div>
                        <!-- /.contacts-list-info -->
                      </a>
                    </li>
                    <!-- End Contact Item -->
                    <li>
                      <a href="#">
                        <img class="contacts-list-img" src="dist/img/user5-128x128.jpg" alt="User Avatar">

                        <div class="contacts-list-info">
                          <span class="contacts-list-name">
                            Nora S. Vans
                            <small class="contacts-list-date float-right">2/10/2015</small>
                          </span>
                          <span class="contacts-list-msg">Where is your new...</span>
                        </div>
                        <!-- /.contacts-list-info -->
                      </a>
                    </li>
                    <!-- End Contact Item -->
                    <li>
                      <a href="#">
                        <img class="contacts-list-img" src="dist/img/user6-128x128.jpg" alt="User Avatar">

                        <div class="contacts-list-info">
                          <span class="contacts-list-name">
                            John K.
                            <small class="contacts-list-date float-right">1/27/2015</small>
                          </span>
                          <span class="contacts-list-msg">Can I take a look at...</span>
                        </div>
                        <!-- /.contacts-list-info -->
                      </a>
                    </li>
                    <!-- End Contact Item -->
                    <li>
                      <a href="#">
                        <img class="contacts-list-img" src="dist/img/user8-128x128.jpg" alt="User Avatar">

                        <div class="contacts-list-info">
                          <span class="contacts-list-name">
                            Kenneth M.
                            <small class="contacts-list-date float-right">1/4/2015</small>
                          </span>
                          <span class="contacts-list-msg">Never mind I found...</span>
                        </div>
                        <!-- /.contacts-list-info -->
                      </a>
                    </li>
                    <!-- End Contact Item -->
                  </ul>
                  <!-- /.contacts-list -->
                </div>
                <!-- /.direct-chat-pane -->
              </div>
              <!-- /.card-body -->
              <div class="card-footer">
                <form action="#" method="post">
                  <div class="input-group">
                    <input type="text" name="message" placeholder="Type Message ..." class="form-control">
                    <span class="input-group-append">
                      <button type="button" class="btn btn-primary">Send</button>
                    </span>
                  </div>
                </form>
              </div>
              <!-- /.card-footer-->
            </div>
            <!--/.direct-chat -->

            <!-- TO DO List -->
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">
                  <i class="ion ion-clipboard mr-1"></i>
                  To Do List
                </h3>

                <div class="card-tools">
                  <ul class="pagination pagination-sm">
                    <li class="page-item"><a href="#" class="page-link">&laquo;</a></li>
                    <li class="page-item"><a href="#" class="page-link">1</a></li>
                    <li class="page-item"><a href="#" class="page-link">2</a></li>
                    <li class="page-item"><a href="#" class="page-link">3</a></li>
                    <li class="page-item"><a href="#" class="page-link">&raquo;</a></li>
                  </ul>
                </div>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <ul class="todo-list" data-widget="todo-list">
                  <li>
                    <!-- drag handle -->
                    <span class="handle">
                      <i class="fas fa-ellipsis-v"></i>
                      <i class="fas fa-ellipsis-v"></i>
                    </span>
                    <!-- checkbox -->
                    <div  class="icheck-primary d-inline ml-2">
                      <input type="checkbox" value="" name="todo1" id="todoCheck1">
                      <label for="todoCheck1"></label>
                    </div>
                    <!-- todo text -->
                    <span class="text">Design a nice theme</span>
                    <!-- Emphasis label -->
                    <small class="badge badge-danger"><i class="far fa-clock"></i> 2 mins</small>
                    <!-- General tools such as edit or delete-->
                    <div class="tools">
                      <i class="fas fa-edit"></i>
                      <i class="fas fa-trash-o"></i>
                    </div>
                  </li>
                  <li>
                    <span class="handle">
                      <i class="fas fa-ellipsis-v"></i>
                      <i class="fas fa-ellipsis-v"></i>
                    </span>
                    <div  class="icheck-primary d-inline ml-2">
                      <input type="checkbox" value="" name="todo2" id="todoCheck2" checked>
                      <label for="todoCheck2"></label>
                    </div>
                    <span class="text">Make the theme responsive</span>
                    <small class="badge badge-info"><i class="far fa-clock"></i> 4 hours</small>
                    <div class="tools">
                      <i class="fas fa-edit"></i>
                      <i class="fas fa-trash-o"></i>
                    </div>
                  </li>
                  <li>
                    <span class="handle">
                      <i class="fas fa-ellipsis-v"></i>
                      <i class="fas fa-ellipsis-v"></i>
                    </span>
                    <div  class="icheck-primary d-inline ml-2">
                      <input type="checkbox" value="" name="todo3" id="todoCheck3">
                      <label for="todoCheck3"></label>
                    </div>
                    <span class="text">Let theme shine like a star</span>
                    <small class="badge badge-warning"><i class="far fa-clock"></i> 1 day</small>
                    <div class="tools">
                      <i class="fas fa-edit"></i>
                      <i class="fas fa-trash-o"></i>
                    </div>
                  </li>
                  <li>
                    <span class="handle">
                      <i class="fas fa-ellipsis-v"></i>
                      <i class="fas fa-ellipsis-v"></i>
                    </span>
                    <div  class="icheck-primary d-inline ml-2">
                      <input type="checkbox" value="" name="todo4" id="todoCheck4">
                      <label for="todoCheck4"></label>
                    </div>
                    <span class="text">Let theme shine like a star</span>
                    <small class="badge badge-success"><i class="far fa-clock"></i> 3 days</small>
                    <div class="tools">
                      <i class="fas fa-edit"></i>
                      <i class="fas fa-trash-o"></i>
                    </div>
                  </li>
                  <li>
                    <span class="handle">
                      <i class="fas fa-ellipsis-v"></i>
                      <i class="fas fa-ellipsis-v"></i>
                    </span>
                    <div  class="icheck-primary d-inline ml-2">
                      <input type="checkbox" value="" name="todo5" id="todoCheck5">
                      <label for="todoCheck5"></label>
                    </div>
                    <span class="text">Check your messages and notifications</span>
                    <small class="badge badge-primary"><i class="far fa-clock"></i> 1 week</small>
                    <div class="tools">
                      <i class="fas fa-edit"></i>
                      <i class="fas fa-trash-o"></i>
                    </div>
                  </li>
                  <li>
                    <span class="handle">
                      <i class="fas fa-ellipsis-v"></i>
                      <i class="fas fa-ellipsis-v"></i>
                    </span>
                    <div  class="icheck-primary d-inline ml-2">
                      <input type="checkbox" value="" name="todo6" id="todoCheck6">
                      <label for="todoCheck6"></label>
                    </div>
                    <span class="text">Let theme shine like a star</span>
                    <small class="badge badge-secondary"><i class="far fa-clock"></i> 1 month</small>
                    <div class="tools">
                      <i class="fas fa-edit"></i>
                      <i class="fas fa-trash-o"></i>
                    </div>
                  </li>
                </ul>
              </div>
              <!-- /.card-body -->
              <div class="card-footer clearfix">
                <button type="button" class="btn btn-primary float-right"><i class="fas fa-plus"></i> Add item</button>
              </div>
            </div>
            <!-- /.card -->
          </section>
          <!-- /.Left col -->
          <!-- right col (We are only adding the ID to make the widgets sortable)-->
          <section class="col-lg-5 connectedSortable">

            <!-- Map card -->
            <div class="card bg-gradient-primary">
              <div class="card-header border-0">
                <h3 class="card-title">
                  <i class="fas fa-map-marker-alt mr-1"></i>
                  Visitors
                </h3>
                <!-- card tools -->
                <div class="card-tools">
                  <button type="button" class="btn btn-primary btn-sm daterange" title="Date range">
                    <i class="far fa-calendar-alt"></i>
                  </button>
                  <button type="button" class="btn btn-primary btn-sm" data-card-widget="collapse" title="Collapse">
                    <i class="fas fa-minus"></i>
                  </button>
                </div>
                <!-- /.card-tools -->
              </div>
              <div class="card-body">
                <div id="world-map" style="height: 250px; width: 100%;"></div>
              </div>
              <!-- /.card-body-->
              <div class="card-footer bg-transparent">
                <div class="row">
                  <div class="col-4 text-center">
                    <div id="sparkline-1"></div>
                    <div class="text-white">Visitors</div>
                  </div>
                  <!-- ./col -->
                  <div class="col-4 text-center">
                    <div id="sparkline-2"></div>
                    <div class="text-white">Online</div>
                  </div>
                  <!-- ./col -->
                  <div class="col-4 text-center">
                    <div id="sparkline-3"></div>
                    <div class="text-white">Sales</div>
                  </div>
                  <!-- ./col -->
                </div>
                <!-- /.row -->
              </div>
            </div>
            <!-- /.card -->

            <!-- solid sales graph -->
            <div class="card bg-gradient-info">
              <div class="card-header border-0">
                <h3 class="card-title">
                  <i class="fas fa-th mr-1"></i>
                  Sales Graph
                </h3>

                <div class="card-tools">
                  <button type="button" class="btn bg-info btn-sm" data-card-widget="collapse">
                    <i class="fas fa-minus"></i>
                  </button>
                  <button type="button" class="btn bg-info btn-sm" data-card-widget="remove">
                    <i class="fas fa-times"></i>
                  </button>
                </div>
              </div>
              <div class="card-body">
                <canvas class="chart" id="line-chart" style="min-height: 250px; height: 250px; max-height: 250px; max-width: 100%;"></canvas>
              </div>
              <!-- /.card-body -->
              <div class="card-footer bg-transparent">
                <div class="row">
                  <div class="col-4 text-center">
                    <input type="text" class="knob" data-readonly="true" value="20" data-width="60" data-height="60"
                           data-fgColor="#39CCCC">

                    <div class="text-white">Mail-Orders</div>
                  </div>
                  <!-- ./col -->
                  <div class="col-4 text-center">
                    <input type="text" class="knob" data-readonly="true" value="50" data-width="60" data-height="60"
                           data-fgColor="#39CCCC">

                    <div class="text-white">Online</div>
                  </div>
                  <!-- ./col -->
                  <div class="col-4 text-center">
                    <input type="text" class="knob" data-readonly="true" value="30" data-width="60" data-height="60"
                           data-fgColor="#39CCCC">

                    <div class="text-white">In-Store</div>
                  </div>
                  <!-- ./col -->
                </div>
                <!-- /.row -->
              </div>
              <!-- /.card-footer -->
            </div>
            <!-- /.card -->

            <!-- Calendar -->
            <div class="card bg-gradient-success">
              <div class="card-header border-0">

                <h3 class="card-title">
                  <i class="far fa-calendar-alt"></i>
                  Calendar
                </h3>
                <!-- tools card -->
                <div class="card-tools">
                  <!-- button with a dropdown -->
                  <div class="btn-group">
                    <button type="button" class="btn btn-success btn-sm dropdown-toggle" data-toggle="dropdown" data-offset="-52">
                      <i class="fas fa-bars"></i>
                    </button>
                    <div class="dropdown-menu" role="menu">
                      <a href="#" class="dropdown-item">Add new event</a>
                      <a href="#" class="dropdown-item">Clear events</a>
                      <div class="dropdown-divider"></div>
                      <a href="#" class="dropdown-item">View calendar</a>
                    </div>
                  </div>
                  <button type="button" class="btn btn-success btn-sm" data-card-widget="collapse">
                    <i class="fas fa-minus"></i>
                  </button>
                  <button type="button" class="btn btn-success btn-sm" data-card-widget="remove">
                    <i class="fas fa-times"></i>
                  </button>
                </div>
                <!-- /. tools -->
              </div>
              <!-- /.card-header -->
              <div class="card-body pt-0">
                <!--The calendar -->
                <div id="calendar" style="width: 100%"></div>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </section>
          <!-- right col -->
        </div>
        <!-- /.row (main row) -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <strong>Copyright &copy; 2014-2021 <a href="https://adminlte.io">AdminLTE.io</a>.</strong>
    All rights reserved.
    <div class="float-right d-none d-sm-inline-block">
      <b>Version</b> 3.2.0
    </div>
  </footer>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

<!-- jQuery -->
<script src="plugins/jquery/jquery.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="plugins/jquery-ui/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button)
</script>
<!-- Bootstrap 4 -->
<script src="plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- ChartJS -->
<script src="plugins/chart.js/Chart.min.js"></script>
<!-- Sparkline -->
<script src="plugins/sparklines/sparkline.js"></script>
<!-- JQVMap -->
<script src="plugins/jqvmap/jquery.vmap.min.js"></script>
<script src="plugins/jqvmap/maps/jquery.vmap.usa.js"></script>
<!-- jQuery Knob Chart -->
<script src="plugins/jquery-knob/jquery.knob.min.js"></script>
<!-- daterangepicker -->
<script src="plugins/moment/moment.min.js"></script>
<script src="plugins/daterangepicker/daterangepicker.js"></script>
<!-- Tempusdominus Bootstrap 4 -->
<script src="plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js"></script>
<!-- Summernote -->
<script src="plugins/summernote/summernote-bs4.min.js"></script>
<!-- overlayScrollbars -->
<script src="plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js"></script>
<!-- AdminLTE App -->
<script src="dist/js/adminlte.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="dist/js/demo.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="dist/js/pages/dashboard.js"></script>
</body>
</html>
```
# STEP 197 COPY FOLDERS Dist AND Plugin FROM Admin-LTE SOURCE IN FOLDER public

# STEP 198 CREATE /resources/views/layouts/ admin.blade.php AND COPY ALL CODE FROM /resources/views/ main.blade.php AND PAST IT

# STEP 199 CREATE FOLDER /resources/views/ admin AND ADD INTO FILE post.blade.php

# STEP 200 CREEATE ROUTE 'admin' WITH NAMESPACE IN routes/web.php AT THE END
```
// LESSON 31
Route::group(['namespace' => 'Admin', 'prefix' => 'admin'], function ()
{
    Route::group(['namespace' => 'Post'], function ()
    {
        Route::get('admin', 'IndexController')->name('admin.post.index');
    });
});
```
# STEP 201 CREATE FOLDERS app\Http\Controllers Admin/Post AND CREATE INTO IndexController.php LOOK AT: http://localhost:81/admin/post
```
<?php

namespace App\Http\Controllers\Admin\Post;

use App\Http\Controllers\Controller;

class IndexController extends Controller
{
    public function __invoke()
    {
        return view('admin.post.index');
    }
}

```
# STEP 202 CREATE FOLDER post IN resources/views/admin AND MOVE INTO FOLDER post post.blade.php AND RENAME ON index.blade.php AND ADD TEST 1111 LOOK AT: http://localhost:81/admin/post AFTER TEST ADD CODE
```
@extends('layouts.admin')

@section('content')
    22222
@endsection
```
# STEP 203 OPEN resources/views/layouts/ admin.blade.php AND FIND STRING: content-wrapper AFTER YOU MUST DELETE CODE FROM STRINGS 873 TO 1449

# STEP 204 AFTER YOU NEED FIND ALL LINKS WITH href="" IN TAG <head></head> AND WRAPP IN {{ assert('') }}
```
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>AdminLTE 3 | Dashboard</title>

  <!-- Google Font: Source Sans Pro -->
  <link rel="stylesheet" href="{{ asset('https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback') }}">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="{{ asset('plugins/fontawesome-free/css/all.min.css') }}">
  <!-- Ionicons -->
  <link rel="stylesheet" href="{{ asset('https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css') }}">
  <!-- Tempusdominus Bootstrap 4 -->
  <link rel="stylesheet" href="{{ asset('plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css') }}">
  <!-- iCheck -->
  <link rel="stylesheet" href="{{ asset('plugins/icheck-bootstrap/icheck-bootstrap.min.css') }}">
  <!-- JQVMap -->
  <link rel="stylesheet" href="{{ asset('plugins/jqvmap/jqvmap.min.css') }}">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{ asset('dist/css/adminlte.min.css') }}">
  <!-- overlayScrollbars -->
  <link rel="stylesheet" href="{{ asset('plugins/overlayScrollbars/css/OverlayScrollbars.min.css') }}">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="{{ asset('plugins/daterangepicker/daterangepicker.css') }}">
  <!-- summernote -->
  <link rel="stylesheet" href="{{ asset('plugins/summernote/summernote-bs4.min.css') }}">
</head>
```
# STEP 205 YOU MUST FIND ALL src=""  AND INSERT assert('')
```src="{{ asset('dist/js/pages/dashboard.js') }}"```
# STEP 206 CREATE TWO FOLDERS IN resources/views/ includes/admin AND ADD INTO FOLDER admin FILE sidebar.blade.php

# STEP 207 DELETE SIDEBAR MENU IN resources/views/layouts/admin.blade.php FROM STRING 208 TO 844 AND INSERT IN resources/views/includes/admin/sidebar.blade.php

# STEP 208 ADD @include('includes.admin.sidebar') IN resources/views/layouts/admin.blade.php
```
<!-- Sidebar Menu -->
        @include('includes.admin.sidebar')
<!-- /.sidebar-menu -->
```
# STEP 209 DELETE CODE FROM 516 TO 635 IN resources/views/layouts/admin.blade.php AFTER DELETE CODE FROM 5 TO 260 ELSE RENAME EXAMPLES ON ADMIN PANEL STRING 6 AFTER DELETE CODE FROM STRIN 16 TO 260 AFTER RENAME Calendar ON Posts STRING 11
# STEP 210 CHANGE TAG <i> 
```<i class="nav-icon fas fa-align-justify"></i>```

```
<li class="nav-header">ADMIN PANEL</li>
          <li class="nav-item">
            <a href="pages/calendar.html" class="nav-link">
              <i class="nav-icon fas fa-align-justify"></i>              
              <p>
                Posts
                <span class="badge badge-info right">2</span>
              </p>
            </a>
          </li>
```
# STEP 211  DELETE DEMO SCRIPTS AT THE END resources/views/layouts/admin.blade.php
```
<!-- AdminLTE for demo purposes -->
<script src="{{ asset('dist/js/demo.js') }}"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="{{ asset('dist/js/pages/dashboard.js') }}"></script>
``` 
# STEP 212  ADD DERECTIVE  @yield('content') IN SECTION CONTENT STRING 237
```
<!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        @yield('content')
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
```
# STEP 213 ADD CODE IN app\Http\Controllers\Admin\Post\IndexController.php
```
<?php

namespace App\Http\Controllers\Admin\Post;

use App\Http\Controllers\Controller;
use App\Http\Filters\PostFilter;
use App\Http\Requests\Post\FilterRequest;
use App\Models\Post;


class IndexController extends Controller
{
    public function __invoke(FilterRequest $request)
    {
        $data = $request->validated();
        $filter = app()
            ->make(
                PostFilter::class,
                ['queryParams' => array_filter($data)]
            );
        $posts = Post::filter($filter)->paginate(10);  
        return view('admin.post.index', compact('posts'));
    }
}
```
# STEP 214 ADD CODE IN resources\views\admin\post\index.blade.php FROM resources\views\post\index.blade.php
```
@extends('layouts.admin')

@section('content')
    <div>
        <div>           
                <a href="{{ route('mypost.create') }}" 
                   class="btn btn-primary mb-3">
                    Create Post
                </a>                       
        </div>
        @foreach ($posts as $post)
            <div style="background: rgb(63, 78, 31)">
            <a href="{{ route('mypost.show', $post->id) }}">
              <b>{{ $post->id }}</b> . {{ $post->title }}
            </a> 
            </div>
        @endforeach

        <div class="mt-3">
            {{ $posts->withQueryString()->links() }}
        </div>
    </div>
@endsection
```
# STEP 215 FINISHED LESSON 31 VIEW ALL POSTS IN span class="badge badge-info right" STRING 12
```<span class="badge badge-info right">{{ $posts->total() }}</span>```
# STEP 216 START LESSON 32 CREATE AUTHORIZATION
```php artisan ui:auth```
# STEP 217 FINISHED LESSON 32 CHANGE ROUTE ->name('home) AT THE END routes\web.php AND COMMENT ROUTE '/' AT THE TOP THE PAGE LOOK AT: http://localhost:81
### TOP PAGE
```
// Route::get('/', function () {
//     return view('welcome');
// });
```
### AT THE END PAGE
```Route::get('/', 'HomeController@index')->name('home');```
# STEP 218 START LESSON 33 CREATE MIGRATION ADD COLUMN ROLE FORE USERS TABLE
```php artisan make:migration add_column_role_to_users_table```

```
<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('role')->default('user')->after('email');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('role');
        });
    }
};
```
# STEP 219 EXECUTE ALL MIGRATIONS
```php artisan migrate```
# STEP 220 CHANGE VALUE MANUALLY IN TABLE users COLUMN ROLE user ON admin
# STEP 221 CREATE MIDDLEWARE AdminPanelMiddleware
```php artisan make:middleware AdminPanelMiddleware```
# STEP 222 ADD dd(auth()); IN public function handle(Request $request, Closure $next) IN App\Http\Middleware\AdminPanelMiddleware.php
```
<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class AdminPanelMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        dump(auth());
        dump(auth()->id());
        dd(auth()->user()->role);
        return $next($request);
    }
}
```
# STEP 223 REGISTRATION AdminPanelmiddleware AT THE END IN App\Http\Kernel.php
```'admin' => AdminPanelMiddleware::class,```
```
<?php

namespace App\Http;

use App\Http\Middleware\AdminPanelMiddleware;
use Illuminate\Foundation\Http\Kernel as HttpKernel;

class Kernel extends HttpKernel
{
    /**
     * The application's global HTTP middleware stack.
     *
     * These middleware are run during every request to your application.
     *
     * @var array<int, class-string|string>
     */
    protected $middleware = [
        // \App\Http\Middleware\TrustHosts::class,
        \App\Http\Middleware\TrustProxies::class,
        \Fruitcake\Cors\HandleCors::class,
        \App\Http\Middleware\PreventRequestsDuringMaintenance::class,
        \Illuminate\Foundation\Http\Middleware\ValidatePostSize::class,
        \App\Http\Middleware\TrimStrings::class,
        \Illuminate\Foundation\Http\Middleware\ConvertEmptyStringsToNull::class,
    ];

    /**
     * The application's route middleware groups.
     *
     * @var array<string, array<int, class-string|string>>
     */
    protected $middlewareGroups = [
        'web' => [
            \App\Http\Middleware\EncryptCookies::class,
            \Illuminate\Cookie\Middleware\AddQueuedCookiesToResponse::class,
            \Illuminate\Session\Middleware\StartSession::class,
            // \Illuminate\Session\Middleware\AuthenticateSession::class,
            \Illuminate\View\Middleware\ShareErrorsFromSession::class,
            \App\Http\Middleware\VerifyCsrfToken::class,
            \Illuminate\Routing\Middleware\SubstituteBindings::class,
        ],

        'api' => [
            // \Laravel\Sanctum\Http\Middleware\EnsureFrontendRequestsAreStateful::class,
            'throttle:api',
            \Illuminate\Routing\Middleware\SubstituteBindings::class,
        ],
    ];

    /**
     * The application's route middleware.
     *
     * These middleware may be assigned to groups or used individually.
     *
     * @var array<string, class-string|string>
     */
    protected $routeMiddleware = [
        'auth' => \App\Http\Middleware\Authenticate::class,
        'auth.basic' => \Illuminate\Auth\Middleware\AuthenticateWithBasicAuth::class,
        'cache.headers' => \Illuminate\Http\Middleware\SetCacheHeaders::class,
        'can' => \Illuminate\Auth\Middleware\Authorize::class,
        'guest' => \App\Http\Middleware\RedirectIfAuthenticated::class,
        'password.confirm' => \Illuminate\Auth\Middleware\RequirePassword::class,
        'signed' => \Illuminate\Routing\Middleware\ValidateSignature::class,
        'throttle' => \Illuminate\Routing\Middleware\ThrottleRequests::class,
        'verified' => \Illuminate\Auth\Middleware\EnsureEmailIsVerified::class,
        'admin' => AdminPanelMiddleware::class,
    ];
}
```
# STEP 224 ADD 'middleware' => 'admin' IN Route::group WITH 'namespace' => 'Admin'
```
// LESSON 31
Route::group([
    'namespace' => 'Admin', 
    'prefix' => 'admin', 
    'middleware' => 'admin'
], function ()
{
    Route::group(['namespace' => 'Post'], function ()
    {
        Route::get('post', 'IndexController')->name('admin.post.index');
    });
});
```
# STEP 225 ADD CODE IN public function handle(Request $request, Closure $next) IN App\Http\Middleware\AdminPanelMiddleware.php
```
<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class AdminPanelMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        if (auth()->user()->role !== 'admin') {
            return redirect()->route('home');
        }
        return $next($request);
    }
}
```
# STEP 226 FINISHED LESSON 33 SWITCH MANUALLY COLUMN role='admin' ON role='user' in TABLE 'users' LOOK AT: http://localhost:81/admin/post
# STEP 227 START LESSON 34 ADD DIRECTIVE @extends('layouts.main') IN resources\views\post\index.blade.php
```
@extends('layouts.myposts')
@extends('layouts.main')
@section('myposts')
    <div>
        <div>           
                <a href="{{ route('mypost.create') }}" 
                   class="btn btn-primary mb-3">
                    Create Post
                </a>                       
        </div>
        @foreach ($posts as $post)
            <div style="background: rgb(63, 78, 31)">
            <a href="{{ route('mypost.show', $post->id) }}">
              <b>{{ $post->id }}</b> . {{ $post->title }}
            </a> 
            </div>
        @endforeach

        <div class="mt-3">
            {{ $posts->withQueryString()->links() }}
        </div>
    </div>
@endsection
```
# STEP 228 ADD NEW NAV ITEM 'Admin' IN resources\views\layouts\main.blade.php LOOK AT: http://localhost:81/myposts
```
<li class="nav-item">
        <a class="nav-link" href="{{ route('admin.post.index') }}">Admin</a>
</li>
```

```
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
    <title>Layout main page</title>
</head>
<body>

    <div class="container">
        <div class="row">
            <nav class="navbar navbar-expand-lg navbar-light bg-light">

  <div class="collapse navbar-collapse" id="navbarNav">
    <ul class="navbar-nav">
      <li class="nav-item active">
        <a class="nav-link" href="{{ route('main.index') }}">Main <span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="{{ route('post.view') }}">Posts</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="{{ route('about.index') }}">About</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="{{ route('contact.index') }}">Contacts</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="{{ route('admin.post.index') }}">Admin</a>
      </li>

    </ul>
  </div>
</nav>
        </div>
    </div>

    <div>
        @yield('content')
    </div>
</body>
</html>
```
# STEP 229 CREATE app\Policies\AdminPolicy.php
```php artisan make:policy AdminPolicy -m User```
# STEP 230 ADD CODE IN public function view(User $user, User $model) IN app\Policies\AdminPolicy.php BAD DESCISION
```
 public function view(User $user, User $model)
    {
        if ($model->role !== 'admin') {
            return false;
        }
    }
```
# STEP 231 ADD CODE IN public function view(User $user, User $model) IN app\Policies\AdminPolicy.php GOOD DESCISION 
```
    /**
     * Determine whether the user can view the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\User  $model
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function view(User $user, User $model)
    {
        return $model->role === 'admin';
    }
```
# STEP 232 ADD CODE IN app\Http\Controllers\Post\IndexController.php 
#### Added code ```$this->authorize('view', auth()->user());```
```
<?php

namespace App\Http\Controllers\Post;

use App\Http\Controllers\Controller;
use App\Http\Filters\PostFilter;
use App\Http\Requests\Post\FilterRequest;
use App\Models\Post;
use Illuminate\Http\Request;

class IndexController extends BaseController
{
    public function __invoke(FilterRequest $request)
    {
        $this->authorize('view', auth()->user());
        $data = $request->validated(); 
        $filter = app()
                 ->make(PostFilter::class, 
                 ['queryParams'=>array_filter($data)]);
        $posts = Post::filter($filter)->paginate(10);        
        
        return view('post.index', compact('posts'));
    }
}
```
# STEP 233 REGISTRATION POLICY IN app\Providers\AuthServiceProvider.php IN protected $policies STRING 19 USER MUST BE AUTHORIZED
```
<?php

namespace App\Providers;

use App\Models\User;
use App\Policies\AdminPolicy;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array<class-string, class-string>
     */
    protected $policies = [
        // 'App\Models\Model' => 'App\Policies\ModelPolicy',
        User::class => AdminPolicy::class,
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        //
    }
}
```
# STEP 224 COMMENT $this->authorize('view', auth()->user()); IN app\Http\Controllers\Post\IndexController.php 
```
<?php

namespace App\Http\Controllers\Post;

use App\Http\Controllers\Controller;
use App\Http\Filters\PostFilter;
use App\Http\Requests\Post\FilterRequest;
use App\Models\Post;
use Illuminate\Http\Request;

class IndexController extends BaseController
{
    public function __invoke(FilterRequest $request)
    {
        //$this->authorize('view', auth()->user());
        $data = $request->validated(); 
        $filter = app()
                 ->make(PostFilter::class, 
                 ['queryParams'=>array_filter($data)]);
        $posts = Post::filter($filter)->paginate(10);        
        
        return view('post.index', compact('posts'));
    }
}
```
# STEP 225 FINISHED LESSON 34 WRAP ITEM NAV LI 'Admin' IN DERECTIVE @can('view', auth()->user()) SWITCH VALUE 'admin' ON 'user' IN COLUMN role IN TABLE 'users' LOOK AT: http://localhost:81/myposts
```
 @can('view', auth()->user())
      <li class="nav-item">
        <a class="nav-link" href="{{ route('admin.post.index') }}">Admin</a>
      </li>
 @endcan
``` 

```
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
    <title>Layout main page</title>
</head>
<body>

    <div class="container">
        <div class="row">
            <nav class="navbar navbar-expand-lg navbar-light bg-light">

  <div class="collapse navbar-collapse" id="navbarNav">
    <ul class="navbar-nav">
      <li class="nav-item active">
        <a class="nav-link" href="{{ route('main.index') }}">Main <span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="{{ route('post.view') }}">Posts</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="{{ route('about.index') }}">About</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="{{ route('contact.index') }}">Contacts</a>
      </li>
      @can('view', auth()->user())
      <li class="nav-item">
        <a class="nav-link" href="{{ route('admin.post.index') }}">Admin</a>
      </li>
      @endcan

    </ul>
  </div>
</nav>
        </div>
    </div>

    <div>
        @yield('content')
    </div>
</body>
</html>
```
# STEP 226 LESSON 35, 36 REST API 

# STEP 227 START LESSON 39 Класс  HTTP Guzzle в Laravel. CREATE FOLDER App\Components AND CREATE CLASS INTO FOLDER ImportDataClient.php AND INSERT CODE
```
<?php

namespace App\Components;

use GuzzleHttp\Client;

class ImportDataClient
{
    public $client;
    
    public function __construct()
    {
        $this->client = new Client([
            // Base URI is used with relative requests
            'base_uri' => 'https://jsonplaceholder.typicode.com/',
            // You can set any number of default request options.
            'timeout'  => 2.0,
            //'verify' => 'false'
        ]);
        
    }
}
```
# STEP 228 CREATE NEW COMMAND 'ImportJsonPlaceholderCommand' LOOK AT app\Console\Commands\ImportJsonPlaceholderCommand.php
```php artisan make:command ImportJsonPlaceholderCommand```
# STEP 229 FINISHED LESSON 39 ADD CODE IN app\Console\Commands\ImportJsonPlaceholderCommand.php
```
<?php

namespace App\Console\Commands;

use App\Components\ImportDataClient;
use App\Models\Post;
use Illuminate\Console\Command;

class ImportJsonPlaceholderCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'import:jsonplaceholder';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get data from jsonplaceholder';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $import = new ImportDataClient();        
        $response = $import->client->request('GET', 'posts');
        //dd(json_decode($response->getBody()->getContents()));
        $data = json_decode($response->getBody()->getContents());

        foreach ($data as $item) {
            //dd($item);
            //dd($item->id);
            Post::firstOrCreate([
                'title' => $item->title
            ], 
            [
                'title' => $item->title,
                'content' => $item->body,
                'category_id' => 2
            ]);
        }

        dd('FINISH!!!');
    }
}
```
# FINISHED PROJECT

















