<?php

namespace App\Http\Controllers;

use App\Models\Post;
use Illuminate\Http\Request;

class PostController extends Controller
{
    public function index()
    {
        $post = Post::find(1);
        $posts = Post::all();
        $isPublished = Post::where('is_published', 0)->first();
        dump($post->title);
        dump($post);
        dump($isPublished->title);
        foreach ($posts as $post) {
            dump($post->title);
        }
        dd($posts);
    }

    public function create()
    {
        $postsArr  = [
            [
                'title' => 'first record',
                'content' => 'first record content',
                'image' => 'first_image.jpg',
                'likes' => 10,
                'is_published' => 1
            ],
            [
                'title' => 'second record',
                'content' => 'second record content',
                'image' => 'second_image.jpg',
                'likes' => 20,
                'is_published' => 1
            ],
            [
                'title' => 'third record',
                'content' => 'third record content',
                'image' => 'third_image.jpg',
                'likes' => 30,
                'is_published' => 1
            ],
        ];

        foreach ($postsArr as $item) {
            //dd($item);
            Post::create($item);
        }
        dd('CREATED!!!');
    }

    public function update()
    {
        $post = Post::find(6);
        //dd($post);
        $post->update([
            'title' => 'updated',
            'content' => 'updated',
            'image' => 'updated.jpg',
            'likes' => 100,
            'is_published' => 0
        ]);
        dd('UPDATED!!!');
    }

    public function delete()
    {
        $post = Post::find(6);
        $post->delete();
        dd('DELETED!!!');
    }

    public function restore()
    {
        $post = Post::withTrashed()->find(6);
        $post->restore();
        dd('RESTORED!!!');
    }

    public function firstOrCreate()
    {
        $anotherPost = [
            'title' => 'another record',
            'content' => 'another record content',
            'image' => 'another_image.jpg',
            'likes' => 50,
            'is_published' => 1
        ];

        $post = Post::firstOrCreate(
            [
                'title' => 'some title'
            ],
            $anotherPost
        );
        dump($post->content);
        dd('FINISHED!!!');
    }

    public function updateOrCreate()
    {
        $anotherPost = [
            'title' => 'some title',
            'content' => 'new record content',
            'image' => 'new_image.jpg',
            'likes' => 150,
            'is_published' => 0
        ];

        $post = Post::updateOrCreate(
            [
                'title' => 'some title'
            ],
            $anotherPost
        );

        dump($post->content);
        dd('FINISHED!!!');

    }

    public function viewPosts()
    {
        $posts = Post::all();
        return view('posts', compact('posts'));
    }
}
