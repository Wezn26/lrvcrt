<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Category2 extends Model
{
    use HasFactory;
    protected $guarded = false;

    public function post2s()
    {
        return $this->hasMany(Post2::class);
    }
}
