<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Post2 extends Model
{
    use HasFactory;
    protected $guarded = false;

    public function category2()
    {
        return $this->belongsTo(Category2::class);
    }

    public function tag2s()
    {
        return $this->belongsToMany(Tag2::class);
    }
}
