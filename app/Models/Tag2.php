<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Tag2 extends Model
{
    use HasFactory;
    protected $guarded = false;

    public function post2s()
    {
        return $this->BelongsToMany(Post2::class);
    }
}
