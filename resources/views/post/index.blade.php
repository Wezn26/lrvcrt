@extends('layouts.myposts')
@extends('layouts.main')
@section('myposts')
    <div>
        <div>           
                <a href="{{ route('mypost.create') }}" 
                   class="btn btn-primary mb-3">
                    Create Post
                </a>                       
        </div>
        @foreach ($posts as $post)
            <div style="background: rgb(63, 78, 31)">
            <a href="{{ route('mypost.show', $post->id) }}">
              <b>{{ $post->id }}</b> . {{ $post->title }}
            </a> 
            </div>
        @endforeach

        <div class="mt-3">
            {{ $posts->withQueryString()->links() }}
        </div>
    </div>
@endsection
