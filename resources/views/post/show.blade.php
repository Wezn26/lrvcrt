@extends('layouts.myposts')
@section('myposts')
    <div>      
            <div style="background: yellowgreen">
              <b>{{ $post->id }}</b> . {{ $post->title }}
            </div>
            <div style="background: olive">
              {{ $post->content }}
            </div>    
    </div>

    <div>
      <h3>
        <a href="{{ route('mypost.edit', $post->id) }}">Edit</a>
      </h3>      
    </div>

    <div>
      <form action="{{ route('mypost.delete', $post->id) }}"        method="post">
        @csrf
        @method('delete')
        <input type="submit" value="Delete" class="btn btn-danger">
      </form>
      <h3>
        <a href="">Delete</a>
      </h3>      
    </div>

    <div>
      <h3>
        <a href="{{ route('mypost.index') }}">Back</a>
      </h3>      
    </div>
@endsection