<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('test', 'TestController@index');

Route::get('posts', 'PostController@index');

Route::get('posts/create', 'PostController@create');

Route::get('posts/update', 'PostController@update');

Route::get('posts/delete', 'PostController@delete');

Route::get('posts/restore', 'PostController@restore');

Route::get('posts/first_or_create', 'PostController@firstOrCreate');

Route::get('posts/update_or_create', 'PostController@updateOrCreate');

Route::get('posts/view', 'PostController@viewPosts')->name('post.view');

//Lesson15
Route::get('main', 'MainController@index')->name('main.index');
Route::get('contacts', 'ContactController@index')
->name('contact.index');
Route::get('about', 'AboutController@index')->name('about.index');

// Lesson 17
// Route::get('myposts', 'MyPostController@index')->name('mypost.index');
// Route::get('myposts/create', 'MyPostController@create')
// ->name('mypost.create');
// Route::post('myposts', 'MyPostController@store')->name('mypost.store');
// Route::get('myposts/{post}', 'MyPostController@show')
// ->name('mypost.show');
// Route::get('myposts/{post}/edit', 'MyPostController@edit')
// ->name('mypost.edit');
// Route::patch('myposts/{post}', 'MyPostController@update')
// ->name('mypost.update');
// Route::delete('myposts/{post}', 'MyPostController@destroy')
// ->name('mypost.delete');

// Lesson 18
Route::get('animals', 'AnimalController@index')->name('animal.index');

// Lesson 24
Route::get('sleep', 'SleepController@index')->name('sleep.index');

// Lesson 25
Route::group(['namespace' => 'Post'], function ()
{
    Route::get('myposts', 'IndexController')->name('mypost.index');
    Route::get('myposts/create', 'CreateController')
    ->name('mypost.create');
    Route::post('myposts', 'StoreController')->name('mypost.store');
    Route::get('myposts/{post}', 'ShowController')
    ->name('mypost.show');
    Route::get('myposts/{post}/edit', 'EditController')
    ->name('mypost.edit');
    Route::patch('myposts/{post}', 'UpdateController')
    ->name('mypost.update');
    Route::delete('myposts/{post}', 'DestroyController')
    ->name('mypost.delete');

});

// LESSON 31
Route::group([
    'namespace' => 'Admin', 
    'prefix' => 'admin', 
    'middleware' => 'admin'
], function ()
{
    Route::group(['namespace' => 'Post'], function ()
    {
        Route::get('post', 'IndexController')->name('admin.post.index');
    });
});
//LESSON 32
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
